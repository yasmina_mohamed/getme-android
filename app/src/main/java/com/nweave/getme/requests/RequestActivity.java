package com.nweave.getme.requests;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.R;
import com.nweave.getme.base.BaseViewBridge;
import com.nweave.getme.business.Constants;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.model.RequestModel;
import com.nweave.getme.model.User;
import com.nweave.getme.requests.adapter.RequestAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestActivity extends AppCompatActivity {
    //    private ArrayList<Contact> contacts;
    private Map<String,User> users;
    private FireBase fireBase;
    private RecyclerView recyclerView;
    private BaseViewBridge baseViewBridge;
    private List<RequestModel> requests;
    private TextView noDataTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        baseViewBridge = new BaseViewBridge(this);
        noDataTextView = findViewById(R.id.no_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Friends' Requests");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        users = new HashMap<>();
        requests = new ArrayList<>();
        setSupportActionBar(toolbar);
        initFirebase();
        initRecyclerView();
        getRequests();
    }

    private void initFirebase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
    }

    private void getRequests() {
        baseViewBridge.showProgressDialog();
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Log.d("uid", "getRequests: " + uid);
        fireBase.getRequestRef().orderByChild(Constants.RECEIVER)
                .equalTo(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                requests.clear();
                users.clear();
                if (dataSnapshot.getChildrenCount() > 0) {
                    recyclerView.setVisibility(View.VISIBLE);
                    noDataTextView.setVisibility(View.GONE);
                    for (DataSnapshot requestSnapshot : dataSnapshot.getChildren()) {
                        RequestModel requestModel = requestSnapshot.getValue(RequestModel.class);
                        String senderId = requestModel.getSenderID();
                        if (senderId != null) {
                            requests.add(requestModel);
                            getSenderInfo(senderId);
                        }
                    }
                } else {
                    noDataTextView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    baseViewBridge.hideProgressDialog();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSenderInfo(final String senderId) {
        fireBase.getUserRef().child(senderId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    users.put(user.getUid(), user);
                    initAdapter(new ArrayList<>(users.values()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.request_recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(mDividerItemDecoration);
    }

    private void initAdapter(List<User> users) {
        baseViewBridge.hideProgressDialog();
        RequestAdapter requestAdapter = new RequestAdapter(users, requests, this);
        recyclerView.setAdapter(requestAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(RequestActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
