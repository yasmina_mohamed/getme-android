package com.nweave.getme.requests.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.model.RequestModel;
import com.nweave.getme.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    private List<User> senders;
    private Context mContext;
    private List<RequestModel> requestModelList;
    HashMap<String,String>mapFriendReceiver=new HashMap<>();
    HashMap<String,String>mapFriendSender=new HashMap<>();


    public RequestAdapter(List<User> senders, List<RequestModel> requestModelList, Context context) {
        this.senders = new ArrayList<>();
        this.senders.addAll(senders);
        this.requestModelList = new ArrayList<>();
        this.requestModelList.addAll(requestModelList);
        mContext = context;
        initFirebase();
    }

    private void initFirebase() {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.request_list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.declineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                removeRequest(pos);
            }
        });

        holder.acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                final FireBase fireBase = FireBase.getFireBaseInstance();
                fireBase.initFirebaseDataBase();
                fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child(Constants.FRIENDS).child(senders.get(pos).getUid()).setValue(senders.get(pos).getUid());
                fireBase.getUserRef().child(senders.get(pos).getUid()).child(Constants.FRIENDS)
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
//                mapFriendSender.put(senders.get(pos).getUid(),senders.get(pos).getUid());
//                mapFriendReceiver.put(FirebaseAuth.getInstance().getCurrentUser().getUid(),FirebaseAuth.getInstance().getCurrentUser().getUid());

//                fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                        .child(Constants.FRIENDS).setValue(mapFriendSender);
//                fireBase.getUserRef().child(senders.get(pos).getUid()).child(Constants.FRIENDS).setValue(mapFriendReceiver);

                removeRequest(pos);
            }
        });


        if (senders.get(position).getProfilePic() != null) {
            Picasso.get().load(senders.get(position).getProfilePic()).placeholder(R.drawable.place_holder_profile).into(holder.userImage);
        } else {
            holder.userImage.setImageResource(R.drawable.place_holder_profile);
        }
        if (senders.get(position).getName() == null || (senders.get(position).getName().isEmpty())) {
            holder.nameTv.setText(senders.get(position).getUserPhone());
        } else {
            holder.nameTv.setText(senders.get(position).getName());
        }

        holder.acceptBtn.setTag(position);
        holder.declineBtn.setTag(position);

    }

    private void removeRequest(int pos) {
        User sender = senders.get(pos);
        RequestModel selected = getRequest(sender.getUid());
        if (selected != null) {
            final FireBase fireBase = FireBase.getFireBaseInstance();
            fireBase.initFirebaseDataBase();
            fireBase.getRequestRef().child(selected.getKey()).removeValue();
            senders.remove(pos);
            requestModelList.remove(selected);
            notifyItemRemoved(pos);
        }
    }

    @Override
    public int getItemCount() {
        return senders.size();
    }


    private RequestModel getRequest(String senderKey) {
        for (RequestModel model : requestModelList) {
            if (model.getSenderID().equals(senderKey)) {
                return model;
            }
        }
        return null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView acceptBtn;
        public TextView declineBtn;
        public TextView nameTv;
        public ImageView userImage;

        public ViewHolder(View itemView) {
            super(itemView);
            acceptBtn = itemView.findViewById(R.id.accept);
            declineBtn = itemView.findViewById(R.id.decline);
            nameTv = itemView.findViewById(R.id.user_name_tv);
            userImage = itemView.findViewById(R.id.user_image_iv);
        }
    }
}
