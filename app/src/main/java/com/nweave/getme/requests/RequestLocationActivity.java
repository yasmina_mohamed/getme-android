package com.nweave.getme.requests;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.PointerIcon;
import android.view.View;
import android.widget.TextView;

import com.nweave.getme.R;
import com.nweave.getme.map.MapsActivity;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.feeeei.circleseekbar.CircleSeekBar;

public class RequestLocationActivity extends AppCompatActivity {

    private CircleSeekBar mSeekbar;
    private TextView requestTv;
    private ArrayList<String> mFriends;
    private int currentTime;
    private com.nweave.getme.business.SharedPreferenceManger manger;
    @BindView(R.id.textview)
    TextView mTextView;

//    @BindView(R.id.friend_img_iv)
//    TextView friendName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_location);
        ButterKnife.bind(this);
        mSeekbar = (CircleSeekBar) findViewById(R.id.seekbar);
        //  mTextView = (TextView) findViewById(R.id.textview);
        requestTv=(TextView)findViewById(R.id.request_btn);

        Intent i = getIntent();
        mFriends = i.getStringArrayListExtra("Friends");
        manger = com.nweave.getme.business.SharedPreferenceManger.getSharedPreferenceMangerInstance();
        mSeekbar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onChanged(CircleSeekBar seekbar, int curValue) {

                curValue = ((int)Math.round(curValue/5))*5;
                mTextView.setText(String.valueOf(curValue));
                currentTime =curValue;
            }
        });

        mSeekbar.setCurProcess(5);
        mSeekbar.setPointerRadius(30);
        requestTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Date date = new Date(System.currentTimeMillis()); //or simply new Date();
                long millis = date.getTime();
                manger.savetime(RequestLocationActivity.this,"Time",millis);
                if(manger.getBoolean(RequestLocationActivity.this,"checkClick")){
                    requestTv.setText("Request");
                    manger.saveBoolean(RequestLocationActivity.this,"checkClick",false);
                } else {
                    manger.saveBoolean(RequestLocationActivity.this,"checkClick",true);
                    requestTv.setText("you are watching");
                }
                Intent intentMap = new Intent(RequestLocationActivity.this,MapsActivity.class);
                intentMap.putExtra("Friends",mFriends);
                intentMap.putExtra("timeRange",currentTime);
                startActivity(intentMap);
            }
        });
    }
}
