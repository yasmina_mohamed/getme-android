package com.nweave.getme.map;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.model.User;
import com.nweave.getme.network.InternetConnection;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

//        https://developers.google.com/location-context/fused-location-provider/

/**
 * Manipulates the map once available.
 * This callback is triggered when the map is ready to be used.
 * This is where we can add markers or lines, add listeners or move the camera. In this case,
 * we just add a marker near Sydney, Australia.
 * If Google Play services is not installed on the device, the user will be prompted to install
 * it inside the SupportMapFragment. This method will only be triggered once the user has
 * installed Google Play services and returned to the app.
 */

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private int MY_LOCATION_REQUEST_CODE = 123;
    private FusedLocationProviderClient mFusedLocationClient;
    private FireBase fireBase;
    private Marker currentUserMarker;
    private ArrayList<String> friendsId;
    private HashMap<String, Marker> friendMarkers;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        friendMarkers = new HashMap<>();
        initFirebase();
        getFriendInfo();
        initMap();
    }


    @OnClick(R.id.arrow_back) void pressBack() {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
//        startLocationUpdates();
    }

    private void checkNetwork() {
        if (InternetConnection.checkConnection(this)) {
            // Internet Available...
        } else {
            // Internet Not Available...
            Toast.makeText(this, "No network connection", Toast.LENGTH_LONG).show();

        }
    }

    private void initFirebase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
    }

    private void getFriendInfo() {
        Intent i = getIntent();
        friendsId = i.getStringArrayListExtra(getString(R.string.friends));
//        Log.i("friend", "onCreate: " + friendLong + "..###.." + friendLong);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkLocationPermission()) {
            mMap.setMyLocationEnabled(true);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
//                                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                                getFriends();
                                setMarkersForCurrentLocation(location);
                                setmLocationCallback();
                                createLocationRequest();
                                startLocationUpdates();
//                                getDirectionBetweenTwoFriends();
                            }
                        }
                    });
        }
    }


    private void setMarkersForCurrentLocation(Location location) {
        saveLocationToFireBase(location);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        if (currentUserMarker == null) {
            currentUserMarker = mMap.addMarker(new MarkerOptions().position(latLng));
            List<LatLng> latLngs = new ArrayList<>();
            for (Map.Entry<String, Marker> entry : friendMarkers.entrySet()) {
                Marker selected = entry.getValue();
                latLngs.add(selected.getPosition());
            }
            latLngs.add(currentUserMarker.getPosition());
            mMap.animateCamera(getCameraPosition(latLngs));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16));
        } else {
            currentUserMarker.setPosition(latLng);
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16));
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void setmLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    setMarkersForCurrentLocation(location);
                }
            }
        };
    }

    private void startLocationUpdates() {
        if (checkLocationPermission()) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback,
                    null /* Looper */);
        }
    }


    private void getFriends() {
        for (String friendId : friendsId) {
            getFriendLocation(friendId);
        }
    }


    private void getFriendLocation(String uid) {
        fireBase.getUserRef().child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                LatLng latLng = getFriendLatLng(dataSnapshot);
                User friend = dataSnapshot.getValue(User.class);
                if (friend != null) {
                    addFriendMarker(friend);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addFriendMarker(User user) {
        if (!friendMarkers.containsKey(user.getUid())) {
            createNewMarker(user);
        } else {
            updateFriendMarkerOrCreateMarkerForNewFriend(user);
        }
    }

    private void updateFriendMarkerOrCreateMarkerForNewFriend(User user) {
        Marker marker = friendMarkers.get(user.getUid());
        marker.setPosition(new LatLng(user.getLatitude(), user.getLongitude()));
    }

    private void createNewMarker(User user) {
        String title;
        if (user.getName() == null || user.getName().isEmpty()) {
            title = user.getUserPhone();
        } else {
            title = user.getName();
        }
        MarkerOptions options = new MarkerOptions().position(new LatLng(user.getLatitude(), user.getLongitude()))
                .title(title).icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(user.getProfilePic())));
        Marker marker = mMap.addMarker(options);
        friendMarkers.put(user.getUid(), marker);
        List<LatLng> latLngs = new ArrayList<>();
        for (Map.Entry<String, Marker> entry : friendMarkers.entrySet()) {
            Marker selected = entry.getValue();
            latLngs.add(selected.getPosition());
        }
        latLngs.add(currentUserMarker.getPosition());
        mMap.animateCamera(getCameraPosition(latLngs));
        Log.d("tag", "" + friendMarkers.size());
    }

    private void saveLocationToFireBase(Location location) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            if (FirebaseAuth.getInstance().getCurrentUser().getUid() != null|!FirebaseAuth.getInstance().getCurrentUser().getUid().isEmpty() ) {
                DatabaseReference userUID = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                userUID.child(Constants.LONGITUDE).setValue(location.getLongitude());
                userUID.child(Constants.LATITUDE).setValue(location.getLatitude());
            }
        }
    }


    private CameraUpdate getCameraPosition(List<LatLng> latLngList) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : latLngList) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        return CameraUpdateFactory.newLatLngBounds(bounds, (int) getResources().getDimension(R.dimen.margin_24));
    }

    private Bitmap getMarkerBitmapFromView(String photoUrl) {
        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_layout, null);
        CircleImageView markerImageView = (CircleImageView) customMarkerView.findViewById(R.id.profile_pic);
        Picasso.get().load(photoUrl).placeholder(R.drawable.place_holder_profile).into(markerImageView);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;

    }

    private Boolean checkLocationPermission() {
        int permissionFineLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionFineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_LOCATION_REQUEST_CODE);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Location Granted", Toast.LENGTH_SHORT)
                        .show();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mMap.setMyLocationEnabled(true);
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
//                                    saveLocationToFireBase(location);
                                    setMarkersForCurrentLocation(location);
                                    getFriends();
                                }

                            }
                        });

            }
        }
    }
}

//    private void getDirectionBetweenTwoFriends() {
//        GoogleDirection.withServerKey("AIzaSyA6DP9VIYtDO1yQWcs_NEQAcptF8RAu3fk")
//                .from(latLng)
//                .to(latLng2)
//                .avoid(AvoidType.FERRIES)
//                .avoid(AvoidType.HIGHWAYS)
//                .execute(new DirectionCallback() {
//                    @Override
//                    public void onDirectionSuccess(Direction direction, String rawBody) {
//                        if (direction.isOK()) {
//
//                            Route route = direction.getRouteList().get(0);
//                            mMap.addMarker(new MarkerOptions().position(latLng));
//                            mMap.addMarker(new MarkerOptions().position(latLng2));
//
//                            ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
//                            mMap.addPolyline(DirectionConverter.createPolyline(getApplicationContext(), directionPositionList, 5, Color.RED));
//                            setCameraWithCoordinationBounds(route);
//
//
//                        } else {
//                            Log.i("direction wrong", "onDirectionSuccess: sth wrong");
//                            // Do something
//
//                        }
//                    }
//
//                    @Override
//                    public void onDirectionFailure(Throwable t) {
//                        // Do something
//                        Log.i("map fail", "onDirectionFailure: " + t.getMessage());
//                    }
//                });

//    }


//    private void setCameraWithCoordinationBounds(Route route) {
//        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
//        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
//        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
//        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
//    }
