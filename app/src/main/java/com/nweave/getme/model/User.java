package com.nweave.getme.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class User implements Parcelable {

    private String uid;
    private String name;
    private String userPhone;
    private String accessToken;
    private String profilePic;
    private String email;
    private double latitude;
    private double longitude;
    private HashMap<String, String> friends = new HashMap<>();



    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid != null ? uid : "");
        result.put("name", name != null ? name : "");
        result.put("userPhone", userPhone != null ? userPhone : "");
        result.put("accessToken", accessToken != null ? accessToken : "");
        result.put("profilePic", profilePic != null ? profilePic : "");
        result.put("email", email != null ? email : "");
        result.put("latitude", latitude );
        result.put("longitude", longitude);
//        result.put("friends", friends);
        return result;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public User() {
    }

    public HashMap<String,String> getFriends() {
        return friends;
    }

    public void setFriends(HashMap<String,String> list) {
        friends.putAll(list);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String phone) {
        this.userPhone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.name);
        dest.writeString(this.userPhone);
        dest.writeString(this.accessToken);
        dest.writeString(this.profilePic);
        dest.writeString(this.email);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeSerializable(this.friends);
    }

    protected User(Parcel in) {
        this.uid = in.readString();
        this.name = in.readString();
        this.userPhone = in.readString();
        this.accessToken = in.readString();
        this.profilePic = in.readString();
        this.email = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.friends = (HashMap<String, String>) in.readSerializable();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
