package com.nweave.getme.model;

public class RequestModel {

    private String senderID;
    private String receiverID;
    private Boolean state;
    private String key;

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String sender) {
        this.senderID = sender;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiver) {
        this.receiverID = receiver;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
