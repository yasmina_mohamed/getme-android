package com.nweave.getme.profile;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nweave.getme.R;
import com.nweave.getme.base.BaseActivity;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.model.User;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

public class UserProfileActivity extends BaseActivity implements View.OnClickListener {
    private View v;
    private ImageView profilePic;
    private EditText userNameEditText;
    private EditText userEmailEditText;
    private StorageReference storageReference;
    private DatabaseReference currentUserRef;
    private User mCurrentUser;
    private Uri selectedImageUri;
    private EditText userPhone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //        setContentView(R.layout.activity_user_profile);
        initElements();
        v = getLayoutInflater().inflate(R.layout.activity_user_profile, activityContainer);
        initFirebase();
        initFirebaseStorage();
        initUserProfileActivityElements();
        getCurrentUserFromSharedPreference();
    }

    private void getCurrentUserFromSharedPreference() {
        SharedPreferenceManger sharedPreferenceManger = SharedPreferenceManger.getSharedPreferenceMangerInstance();
        if ((sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER) != null)) {
            mCurrentUser = sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER);
            showCurrentUserData();
            showUserDataToNavigationViewHeader(mCurrentUser);
        }

    }

    private void initFirebase() {
        FireBase fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        currentUserRef = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }


    private void showCurrentUserData() {
        if (mCurrentUser != null) {
            showUserName();
            showUserEmail();
            showUserImage();
            showUserPhone();
        }
    }

    private void showUserName() {
        if (mCurrentUser.getName() != null) {
            userNameEditText.setText(mCurrentUser.getName());
        } else {
            userEmailEditText.setHint("Name");
        }
    }

    private void showUserEmail() {
        if (mCurrentUser.getEmail() != null) {
            userEmailEditText.setText(mCurrentUser.getEmail());
        } else {
            userEmailEditText.setHint("Email");
        }

    }

    private void showUserImage() {
        Picasso.get().load(mCurrentUser.getProfilePic()).placeholder(R.drawable.place_holder_profile).into(profilePic);
    }


    private void showUserPhone() {
        userPhone.setEnabled(false);
        userPhone.setText(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
    }

    private void initFirebaseStorage() {
//           create firebase storage instance
        FirebaseStorage storage = FirebaseStorage.getInstance();
//           it is a pointer to file in the cloude
        storageReference = storage.getReference();
    }

    private void initUserProfileActivityElements() {
        userNameEditText = v.findViewById(R.id.user_name);
        userEmailEditText = v.findViewById(R.id.user_email);
        profilePic = v.findViewById(R.id.profile_pic);
        Button saveButton = v.findViewById(R.id.save_btn);
        saveButton.setOnClickListener(this);
        profilePic.setOnClickListener(this);
        userPhone = v.findViewById(R.id.user_phone);
    }


    //----------------------------upload image-------------------------------------------
    private void uploadImageToFirebase(Uri selectedImageUri) {
        if (selectedImageUri != null) {
            showProgressDialog();
            String id = FirebaseAuth.getInstance().getUid();
            final StorageReference ref = storageReference.child("images/" + id);
            UploadTask uploadFile = ref.putFile(selectedImageUri);
            uploadFile.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    // Continue with the task to get the download URL
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        mCurrentUser.setProfilePic(downloadUri.toString());
                        saveCurrentUserOnSharedPreference();
                        showUserDataToNavigationViewHeader(mCurrentUser);
                        saveUserFirebase();

                    } else {
                        Toast.makeText(UserProfileActivity.this, R.string.upload_error, Toast.LENGTH_SHORT).show();
                        hideProgressDialog();

                    }
                }
            });

        }
    }

    //-----------------------------------on click-----------------------------------------------------
    private boolean dataChanged = false;

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.profile_pic:
                startGetImage();
                break;

            case R.id.save_btn:
                showProgressDialog();
                String userEmail = userEmailEditText.getText().toString();
                if (isValidEmail(userEmail)) {
                    mCurrentUser.setEmail(userEmail);
                    dataChanged = true;

                } else {
                    if (!userEmail.isEmpty()) {
                        //dataChanged= false;
                        Toast.makeText(this, getString(R.string.email_error), Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                if (userNameEditText.getText().toString().equals("") || userNameEditText.getText().toString().isEmpty()) {
                    userNameEditText.setError("Name is required!");
                    dataChanged = false;
                    return;
                } else {
                    dataChanged = true;
                    String userName = userNameEditText.getText().toString();
                    mCurrentUser.setName(userName);
                }

                if (selectedImageUri != null) {
                    dataChanged = true;
                    uploadImageToFirebase(selectedImageUri);
                } else {
                    saveCurrentUserOnSharedPreference();
                    showUserDataToNavigationViewHeader(mCurrentUser);
                    saveUserFirebase();
                }
        }
    }

    private boolean isValidEmail(CharSequence target) {
        return Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    private void saveUserFirebase() {
        currentUserRef.setValue(mCurrentUser)/*updateChildren(mCurrentUser.toMap())*/.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isComplete()) {
//                    saveCurrentUserOnSharedPreference();
//                    showUserDataToNavigationViewHeader(mCurrentUser);
                    Toast.makeText(getApplicationContext(), "data is saved", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(UserProfileActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }

                hideProgressDialog();
            }
        });
    }
//    ---------------------------------update sharedPreferenc--------------------------------------

    private void saveCurrentUserOnSharedPreference() {
        SharedPreferenceManger.getSharedPreferenceMangerInstance().saveCurrentUser(UserProfileActivity.this, mCurrentUser);
    }

//    ---------------------------------get image--------------------------------------------------

    private void startGetImage() {
        if (isStoragePermissionGranted()) {
            CropImage.activity().setAspectRatio(7, 7)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                profilePic.setImageURI(resultUri);
                String path = resultUri.getPath(); // “/mnt/sdcard/FileName.mp3”
                selectedImageUri = Uri.fromFile(new File(path));
                Log.i("success user profile", "onActivityResult: ");

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.i("error User Profile", "onActivityResult: " + error.getMessage());
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v("permission", "Permission: " + permissions[0] + "was " + grantResults[0]);
                startGetImage();
            }
        }
    }

    protected boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                Log.v("permission", "Permission is granted");
                return true;
            } else {
                Log.v("permission", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("permission", "Permission is granted");
            return true;
        }
    }
}
