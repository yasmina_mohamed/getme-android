package com.nweave.getme.contacts.view;

import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public interface MyCallback {
    void onCallback(ArrayList<Contact> value);
}
