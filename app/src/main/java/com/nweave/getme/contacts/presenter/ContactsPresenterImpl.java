package com.nweave.getme.contacts.presenter;

import android.app.Activity;

import com.nweave.getme.contacts.view.ContactsViewerInterface;
import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public class ContactsPresenterImpl implements ContactsPresenterInterface {
    private Activity mContext;
    private ContactsViewerInterface mViewerInterface;

    public ContactsPresenterImpl(Activity context, ContactsViewerInterface viewerInterface) {
        mContext=context;
        mViewerInterface=viewerInterface;
        getContactsFromContentProvider();
    }

    private void getContactsFromContentProvider()
    {
        new ContactsContentProvider(mContext,this);
    }


    @Override
    public void getContactFromProvider(ArrayList<Contact>contacts) {
        mViewerInterface.viewContacts(contacts);
    }
}
