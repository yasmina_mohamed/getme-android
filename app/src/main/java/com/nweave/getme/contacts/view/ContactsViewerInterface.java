package com.nweave.getme.contacts.view;

import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public interface ContactsViewerInterface {
    void viewContacts(ArrayList<Contact>contacts);
}
