package com.nweave.getme.contacts.presenter;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public class ContactsContentProvider  {
    private Context mContext;
    private ContactsPresenterInterface mContactsPresenter;
//    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;


    public ContactsContentProvider(Context context, ContactsPresenterImpl contactsPresenter) {
        mContext = context;
        mContactsPresenter = contactsPresenter;
        readContacts();
    }

    private void readContacts(){
        new LoadContacts().execute();
    }

    private class LoadContacts extends AsyncTask<Void, Void, ArrayList<Contact>> {

        @Override
        protected ArrayList<Contact> doInBackground(Void... voids) {
            return getContacts();
        }

        @Override
        protected void onPostExecute(ArrayList<Contact> contacts) {
            super.onPostExecute(contacts);
            if(contacts!=null){
                mContactsPresenter.getContactFromProvider(contacts);
            }
        }
    }

    private ArrayList<Contact> getContacts() {

        // Get URI
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        // Set the columns that you want to get from that
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.PHOTO_URI,
//                ContactsContract.CommonDataKinds.Email.ADDRESS
        };


        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        ContentResolver cr = mContext.getContentResolver();
        Cursor cursor =cr.query(uri, projection, null, null, sortOrder);
        ArrayList<Contact> contacts = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Contact contact = new Contact();
                contact.setName(cursor.getString(cursor
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                contact.setPhone(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                contact.setProfilePic(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
                contacts.add(contact);
            }
        }
        return contacts;
    }



}


