package com.nweave.getme.contacts.view;

import android.view.View;

import com.nweave.getme.model.Contact;

import java.util.List;

public interface SmsCallback {
    void sendSms(Contact contact, List<Contact> invitedContacts, View v);
}
