package com.nweave.getme.contacts.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

public class Permission extends Activity{
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private Context mContext;
    public Permission(Context context) {
        mContext=context;
    }

    public  void getPermission(){

        int hasReadContactsPermission = mContext.checkSelfPermission(Manifest.permission.READ_CONTACTS);
        if (hasReadContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CODE_ASK_PERMISSIONS);

//            return;
        } else {
//            new ContactsContentProvider.loadContacts().execute();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    new ContactsContentProvider.loadContacts().execute();
                    Toast.makeText(mContext, "Read_CONTACTS accepted", Toast.LENGTH_SHORT)
                            .show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // Permission Denied
                    Toast.makeText(mContext, "WRITE_CONTACTS Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            }

        }
    }
}
