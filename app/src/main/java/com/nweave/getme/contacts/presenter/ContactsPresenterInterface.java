package com.nweave.getme.contacts.presenter;

import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public interface ContactsPresenterInterface {
    void getContactFromProvider(ArrayList<Contact>contacts);
}
