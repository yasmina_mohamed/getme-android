package com.nweave.getme.contacts.adapter;

import com.nweave.getme.model.Contact;

public interface FriendDuplicationChecker{
    void getFriendDuplicationResult(Contact contact,Boolean exist);
}
