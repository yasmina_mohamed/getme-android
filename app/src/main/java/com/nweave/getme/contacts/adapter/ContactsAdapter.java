package com.nweave.getme.contacts.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.contacts.view.SmsCallback;
import com.nweave.getme.firebase.operation.FirebaseDataOperation;
import com.nweave.getme.model.Contact;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> implements View.OnClickListener, FriendDuplicationChecker {

    private ArrayList<Contact> mContacts = new ArrayList<>();
    private Context mContext;
    private FirebaseDataOperation firebaseDataOperation;
    private SharedPreferenceManger pref;
    private SmsCallback smsCallback;


    public ContactsAdapter(ArrayList<Contact> contacts, Context context, SmsCallback smsCallback) {
        mContacts.addAll(contacts);
        mContext = context;
        pref = SharedPreferenceManger.getSharedPreferenceMangerInstance();
        this.smsCallback=smsCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.contacts_list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsAdapter.ViewHolder holder, int position) {

        holder.nameTxtView.setText(mContacts.get(position).getName());
        holder.phoneTxtView.setText(mContacts.get(position).getPhone());
        if (isExist(mContacts.get(position))) {
            holder.addFriendBtn.setVisibility(View.INVISIBLE);
            holder.removeContactBtn.setVisibility(View.INVISIBLE);
            holder.doneImageView.setVisibility(View.VISIBLE);
        } else {
            holder.addFriendBtn.setVisibility(View.VISIBLE);
            holder.removeContactBtn.setVisibility(View.VISIBLE);
            holder.doneImageView.setVisibility(View.GONE);
        }
        setFriendProfilePic(holder, position);
        setListener(holder, position);
    }


    private void setFriendProfilePic(ContactsAdapter.ViewHolder holder, int position) {
        if (mContacts.get(position).getProfilePic() == null) {
            holder.profilePic.setImageResource(R.drawable.place_holder_profile);
        } else {
            Picasso.get().load(mContacts.get(position).getProfilePic()).into(holder.profilePic);
        }
    }

    private void setListener(ContactsAdapter.ViewHolder holder, int position) {
        holder.removeContactBtn.setOnClickListener(this);
        holder.removeContactBtn.setTag(R.string.position, position);
        holder.addFriendBtn.setOnClickListener(this);
        holder.addFriendBtn.setTag(R.string.position, position);
        holder.addFriendBtn.setTag(R.string.invited,holder.invitedBtn);
    }

    private boolean isExist(Contact contact) {
        List<Contact> invitedContacts = SharedPreferenceManger.getSharedPreferenceMangerInstance().getDataFromSharedPreference(mContext, Constants.FRIENDS_CONTACTS);
        if (invitedContacts == null || invitedContacts.isEmpty()) {
            return false;
        }
        for (Contact selectedContact : invitedContacts) {
            if (selectedContact.getPhone().equals(contact.getPhone())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag(R.string.position);
        Contact contact = mContacts.get(pos);
        ImageView invitedContactBtn = (ImageView) v.getTag(R.string.invited);
        switch (v.getId()) {
            case R.id.contact_screen_add_item:
                List<Contact> invitedContacts = SharedPreferenceManger.getSharedPreferenceMangerInstance().getDataFromSharedPreference(mContext, Constants.FRIENDS_CONTACTS);
                if (invitedContacts == null) {
                    invitedContacts = new ArrayList<>();
                }
                v.setVisibility(View.INVISIBLE);
                invitedContactBtn.setVisibility(View.VISIBLE);
                invitedContacts.add(contact);
                smsCallback.sendSms(contact,invitedContacts,v);
//                notifyItemRangeChanged(pos, mContacts.size());
                break;

            case R.id.contacts_screen_remove_item:
                List<Contact> removedContacts = SharedPreferenceManger.getSharedPreferenceMangerInstance().getDataFromSharedPreference(mContext, Constants.REMOVED_CONTACTS);
                if (removedContacts == null) {
                    removedContacts = new ArrayList<>();
                }
                removedContacts.add(contact);
                SharedPreferenceManger.getSharedPreferenceMangerInstance().saveContactList(removedContacts, mContext, Constants.REMOVED_CONTACTS);
                mContacts.remove(contact);
                notifyItemRemoved(pos);
                notifyItemRangeChanged(pos, mContacts.size());
                Toast.makeText(mContext, "the contact removed", Toast.LENGTH_LONG).show();
                break;
        }

    }

//    public void sendSmsByManager(Contact contact) {
//        try {
//            // Get the default instance of the SmsManager
//            String phone=contact.getPhone().replace(" ","");
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(phone,
//                    "https://play.google.com/store/apps/details?id=com.nweave.getme",
//                    "hi buddy install Get Me app https://play.google.com/store/apps/details?id=com.nweave.getme",
//                    null,
//                    null);
//            Toast.makeText(mContext, "Your sms has successfully sent!", Toast.LENGTH_LONG).show();
//
//        } catch (Exception ex) {
//            Toast.makeText(mContext, "Your sms has failed...", Toast.LENGTH_LONG).show();
//            Log.d("sms", "sendSmsByManager: " + ex.getMessage());
//        }
//    }



    @Override
    public void getFriendDuplicationResult(Contact contact, Boolean exist) {

        if (exist) {
            Toast.makeText(mContext, R.string.friends_already_exist, Toast.LENGTH_LONG).show();

        } else {

        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTxtView;
        public TextView phoneTxtView;
        public ImageView profilePic;
        public ImageView addFriendBtn;
        public ImageView invitedBtn;
        public ImageView removeContactBtn;
        public ImageView doneImageView;
        public View view;

        ViewHolder(View itemView) {
            super(itemView);
            nameTxtView = itemView.findViewById(R.id.contact_name);
            phoneTxtView = itemView.findViewById(R.id.contact_phone);
            profilePic = itemView.findViewById(R.id.imageView);
            addFriendBtn = itemView.findViewById(R.id.contact_screen_add_item);
            removeContactBtn = itemView.findViewById(R.id.contacts_screen_remove_item);
            doneImageView = itemView.findViewById(R.id.contact_screen_done_item);
            invitedBtn=itemView.findViewById(R.id.contact_screen_sent_item);
            view = itemView;
        }
    }

    public void setFilter(ArrayList<Contact> contact) {
        mContacts = new ArrayList<>();
        mContacts.addAll(contact);
        notifyDataSetChanged();
    }
}
