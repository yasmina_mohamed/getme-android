package com.nweave.getme.contacts.view;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nweave.getme.R;
import com.nweave.getme.base.BaseActivity;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.business.search.ContactSearch;
import com.nweave.getme.business.search.ContactSearchResult;
import com.nweave.getme.contacts.adapter.ContactsAdapter;
import com.nweave.getme.contacts.presenter.ContactsPresenterImpl;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.model.Contact;
import com.nweave.getme.model.User;

import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends BaseActivity implements ContactsViewerInterface, ContactSearchResult, MyCallback, View.OnTouchListener, SmsCallback {
    private static final int MY_PERMISSIONS_REQUEST_ACCOUNTS = 1;
    private ArrayList<Contact> mContacts;
    private RecyclerView recyclerView;
    private ContactsAdapter contactsAdapter;
    private ArrayList<Contact> removedContacts;
    private ArrayList<Contact> friendsContacts;
    private View v;
    private int counter = 0;
    private SearchView searchView;
    private ArrayList<Contact> firebaseContacts;
    private FireBase fireBase;
    private TextView noDataTextView;
    private ConstraintLayout constraintLayout;
    private User mCurrentUser;
    private SharedPreferenceManger sharedPreferenceManger;
    private boolean permissionIsGranted = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initElements();
        v = getLayoutInflater().inflate(R.layout.activity_contacts, activityContainer);
        showUserDataToNavigationViewHeader(SharedPreferenceManger.getSharedPreferenceMangerInstance().getCurrentUser(this, Constants.CURRENT_USER));
        initContactActivityElements();
        initRecyclerView();
        initFirebase();
        checkMultiPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgressDialog();
        getSharedPref();
        getCurrentUserFromSharedPreference();
        if (permissionIsGranted) {
            initContactPresenter();
        } else {
            setContactsVisibile(false);
            hideProgressDialog();
        }
    }


    private void getCurrentUserFromSharedPreference() {
        if ((sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER) != null)) {
            mCurrentUser = sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER);
            showUserDataToNavigationViewHeader(mCurrentUser);
        }
    }

    private void initContactActivityElements() {
        sharedPreferenceManger = SharedPreferenceManger.getSharedPreferenceMangerInstance();
        noDataTextView = findViewById(R.id.no_data);
        searchView = findViewById(R.id.contact_search);
        constraintLayout = findViewById(R.id.contacts_activity_layout);
//        constraintLayout.setOnTouchListener(this);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

    }

    private void initFirebase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
    }


    private void initRecyclerView() {
        recyclerView = v.findViewById(R.id.contacts_recyclerView);
        recyclerView.setHasFixedSize(true);
//        recyclerView.setOnTouchListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(mDividerItemDecoration);
    }

    private void getSharedPref() {
        removedContacts = new ArrayList<>();
        friendsContacts = new ArrayList<>();
        if (sharedPreferenceManger.getDataFromSharedPreference
                (this, Constants.REMOVED_CONTACTS) != null) {
            removedContacts.addAll(sharedPreferenceManger.
                    getDataFromSharedPreference(this, Constants.REMOVED_CONTACTS));
        }
        if (sharedPreferenceManger.getDataFromSharedPreference
                (this, Constants.FRIENDS_CONTACTS) != null) {
            friendsContacts.addAll(sharedPreferenceManger.
                    getDataFromSharedPreference(this, Constants.FRIENDS_CONTACTS));
        }

    }

    //-------------------------------------Permission-------------------------------------

    //    private void readContacts() {
//        if (Build.VERSION.SDK_INT < 23) {
//            initContactPresenter();
//        } else {
//            if (checkAndRequestPermissions()) {
//                //If you have already permitted the permission
//                initContactPresenter();
//            }
//        }
//    }
//


//    private boolean checkAndRequestPermissions() {
//        int permissionReadContact = ContextCompat.checkSelfPermission(this,
//                Manifest.permission.READ_CONTACTS);
//
//        int permissionSMS = ContextCompat.checkSelfPermission(this,
//                Manifest.permission.SEND_SMS);
//
//        List<String> listPermissionsNeeded = new ArrayList<>();
//        if (permissionReadContact != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
//        }
//        if (permissionSMS != PackageManager.PERMISSION_GRANTED) {
//            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
//        }
//        if (!listPermissionsNeeded.isEmpty()) {
//            ActivityCompat.requestPermissions(this,
//                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_ACCOUNTS);
//            return false;
//        }
//
//        return true;
//    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_ACCOUNTS:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    int permissionReadContact = ContextCompat.checkSelfPermission(this,
//                            Manifest.permission.READ_CONTACTS);
//                    if (permissionReadContact == PackageManager.PERMISSION_GRANTED) {
//                        initContactPresenter();
//                    } else {
//                        setContactsVisibile(false);
//                    }
//                    //Permission Granted Successfully. Write working code here.
//                } else {
//                    //You did not accept the request can not use the functionality.
////                     Permission Denied
//                    Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_SHORT)
//                            .show();
//                }
//                break;
//        }
//    }

    private void setContactsVisibile(boolean isVisible) {
        if (isVisible) {
            recyclerView.setVisibility(View.VISIBLE);
            noDataTextView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            noDataTextView.setVisibility(View.VISIBLE);
        }
    }

    //---------------------------------startPresenterToGetContact-----------------------------------

    private void initContactPresenter() {
        new ContactsPresenterImpl(this, this);
    }
    //-------------------------------ViewContacts------------------------------------------------------------

    @Override
    public void viewContacts(ArrayList<Contact> contacts) {
        if (contacts.size() > 0) {
            if (mContacts == null) {
                mContacts = new ArrayList<>();
            } else {
                mContacts.clear();
            }
            mContacts.addAll(contacts);
            if (removedContacts.size() > 0) {
                removeBlockedContactFromMyContacts();
            }
            removeFirebaseUserFromMyContacts(this);
        } else {
            setContactsVisibile(false);
            hideProgressDialog();
        }

    }

    private void removeFirebaseUserFromMyContacts(final MyCallback myCallback) {
        if (firebaseContacts == null) {
            firebaseContacts = new ArrayList<>();
        } else {
            firebaseContacts.clear();
        }
        counter = 0;
        for (final Contact firebaseUser : mContacts) {
            String phone = firebaseUser.getPhone().replace(" ", "");
            fireBase.getUserRef().orderByChild(Constants.USER_PHONE).equalTo("+2" + phone)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                firebaseContacts.add(firebaseUser);
                            }
                            counter++;
                            if (counter == mContacts.size()) {
                                mContacts.removeAll(firebaseContacts);
                                hideProgressDialog();
                                myCallback.onCallback(mContacts);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }

                    });
        }
    }


    private void removeBlockedContactFromMyContacts() {
        // we decrease the counter couz arraylist index when deleted the subsequent element shifts to the left

        List localeContacts = new ArrayList();
        for (Contact contact : mContacts) {
            String contactName = contact.getName();
            for (int removedContactIndex = 0; removedContactIndex < removedContacts.size(); removedContactIndex++) {
                String removedContactName = removedContacts.get(removedContactIndex).getName();
                if (contactName.equals(removedContactName)) {
                    localeContacts.add(contact);
                }
            }
        }
        mContacts.removeAll(localeContacts);

    }


//    ------------------------show contacts in recycler view---------------------------------------------

    private void initAdapter() {
        if (mContacts != null && !mContacts.isEmpty()) {
            setContactsVisibile(true);
            contactsAdapter = new ContactsAdapter(mContacts, this, this);
            recyclerView.setAdapter(contactsAdapter);
        } else {
            setContactsVisibile(false);
        }
    }

//   ------------------------------search-------------------------------------------

    private void initSearch() {
        new ContactSearch(this, searchView, this, mContacts);
    }

    @Override
    public void getSearchResult(ArrayList<Contact> searchItem) {
        contactsAdapter.setFilter(searchItem);
    }


    @Override
    public void onCallback(ArrayList<Contact> value) {
        initAdapter();
        initSearch();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hideKeyboard(v);
        return false;
    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (in != null) {
            in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        searchView.clearFocus();
    }

    private void checkMultiPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.SEND_SMS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Log.d("test", "onPermissionsChecked: ");
                            permissionIsGranted = true;

                        } else {
                            hideProgressDialog();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void sendSms(Contact contact, final List<Contact> invitedContacts, final View v) {
        final ImageView invitedContactBtn = (ImageView) v.getTag(R.string.invited);

        try {
            final SharedPreferenceManger pref = SharedPreferenceManger.getSharedPreferenceMangerInstance();
            String messageNumber = contact.getPhone().replace(" ", "");
            String messageText = "hi buddy install Get Me app https://play.google.com/store/apps/details?id=com.nweave.getme";
            String sent = "SMS_SENT";
//            showProgressDialog();
            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                    new Intent(sent), 0);

            //---when the SMS has been sent---
            registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    if (getResultCode() == Activity.RESULT_OK) {
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        pref.saveContactList(invitedContacts, getApplicationContext(), Constants.FRIENDS_CONTACTS);
                        contactsAdapter.notifyDataSetChanged();
                        invitedContactBtn.setVisibility(View.GONE);
//                        hideProgressDialog();
                    } else {
                        Toast.makeText(getBaseContext(), "SMS could not sent",
                                Toast.LENGTH_SHORT).show();
                        v.setVisibility(View.VISIBLE);
                        invitedContactBtn.setVisibility(View.GONE);
//                        hideProgressDialog();
                    }
                }
            }, new IntentFilter(sent));

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(messageNumber, null, messageText,sentPI, null);

        } catch (Exception ex) {
            Toast.makeText(this, "Your sms has failed...", Toast.LENGTH_LONG).show();
            Log.d("sms", "sendSmsByManager: " + ex.getMessage());
            v.setVisibility(View.VISIBLE);
            invitedContactBtn.setVisibility(View.GONE);
            hideProgressDialog();
        }
    }

}
