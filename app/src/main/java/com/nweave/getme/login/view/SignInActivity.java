package com.nweave.getme.login.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.firebase.operation.FirebaseDataOperation;

public class SignInActivity extends AppCompatActivity {


//    @BindView(R.id.signin_name_et)
//    TextInputEditText nameEt;

//
//    @BindView(R.id.name_layout)
//    TextInputLayout nameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.signin_layout) void navigateToPhoneLoginScreen(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


//    private boolean isValid(){
//        boolean isNameEmpty = true ;
//        if(nameEt.getText().toString().equals("") && !(nameEt.getText().toString()).isEmpty() ){
//            isNameEmpty = false;
//            nameEt.setError(getString(R.string.app_error_name));
//        }
//        return isNameEmpty;
//    }
}
