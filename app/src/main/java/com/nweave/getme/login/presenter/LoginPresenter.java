package com.nweave.getme.login.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.firebase.operation.FirebaseDataOperation;
import com.nweave.getme.login.view.ILoginView;
import com.nweave.getme.login.view.InitActivityInterface;
import com.nweave.getme.model.User;

public class LoginPresenter implements ILoginPresenter {
    private FireBase fireBase;
    private InitActivityInterface mInitActivityInterface;
    private String token;
    private Context mContext;

    private ILoginView loginView;

    public LoginPresenter(Context mContext , ILoginView loginView) {
        this.mContext = mContext;
        this.loginView =loginView;
        initFireBase();
    }

//    public LoginPresenter(InitActivityInterface initActivityInterface, Context context) {
//        mInitActivityInterface = initActivityInterface;
//        mContext = context;
//        initFireBase();
//        checkUserExistenceInFirebaseAndStartActivity();
//    }

    private void initFireBase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
    }
//
//    private void checkUserExistenceInFirebaseAndStartActivity() {
//        Query query = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    FirebaseDataOperation firebaseDataOperation = new FirebaseDataOperation();
//                    firebaseDataOperation.updateUserAccessToken(mContext);
//                    mInitActivityInterface.initActivity();
//
//                } else {
//                    saveUserToDataBase();
//                    mInitActivityInterface.initActivity();
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }

//    private void saveUserToDataBase() {
//        User user = new User();
//        user.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
//        user.setUserPhone(FirebaseAuth.getInstance().getCurrentUser()
//                .getPhoneNumber());
//        user.setAccessToken(SharedPreferenceManger.getSharedPreferenceMangerInstance().getAccessToken(mContext));
////        user.setName(username);
//        fireBase.getUserRef().child(user.getUid()).setValue(user);
//    }

    @Override
    public void checkUserExistenceInFirebase() {
        Query query = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    FirebaseDataOperation firebaseDataOperation = new FirebaseDataOperation();
                    firebaseDataOperation.updateUserAccessToken(mContext);
                    loginView.initHomeScreen();

                } else {
                    //saveUserToDataBase();
                    loginView.initProfileScreen();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void saveUserToDbAfterChecked() {
        User user = new User();
        user.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
        user.setUserPhone(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
        user.setAccessToken(SharedPreferenceManger.getSharedPreferenceMangerInstance().getAccessToken(mContext));
//        user.setName(username);
        fireBase.getUserRef().child(user.getUid()).setValue(user);
        SharedPreferenceManger.getSharedPreferenceMangerInstance().saveCurrentUser(mContext, user);
//        loginView.initLoginProcess();
    }
}
