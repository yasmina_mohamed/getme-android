package com.nweave.getme.login.view;

public interface ILoginView {
    void initProfileScreen();
    void initHomeScreen();
    void initLoginProcess();
}
