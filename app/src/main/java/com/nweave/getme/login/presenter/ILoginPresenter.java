package com.nweave.getme.login.presenter;

public interface ILoginPresenter {
        public void checkUserExistenceInFirebase();
        public void saveUserToDbAfterChecked();

}
