package com.nweave.getme.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.nweave.getme.R;
import com.nweave.getme.UserNameActivity;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.login.presenter.ILoginPresenter;
import com.nweave.getme.login.presenter.LoginPresenter;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements InitActivityInterface, ILoginView {
    private static final int RC_SIGN_IN = 123;
    private String username;
    private ILoginPresenter loginPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAuthenticationProvider();
        Intent intent = getIntent();
        loginPresenter = new LoginPresenter(this, this);

        if (intent != null) {
            username = intent.getStringExtra("username");
        }

    }

    private void initAuthenticationProvider() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setTheme(R.style.LoginTheme)
                        .build(),
                RC_SIGN_IN);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                // Successfully signed in
                loginPresenter.checkUserExistenceInFirebase();
//                new LoginPresenter(this,this);
            } else {
                Toast.makeText(this, R.string.signinfailed, Toast.LENGTH_LONG).show();
                finish();
//                Log.i("test2", "onActivityResult: " + response.getError());
            }
        }
    }
    // [END auth_fui_result]


    //
    @Override
    public void initActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void initProfileScreen() {
        loginPresenter.saveUserToDbAfterChecked();
        Intent intent = new Intent(this, UserNameActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void initHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void initLoginProcess() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}

