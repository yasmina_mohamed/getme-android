package com.nweave.getme.business.search;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Toast;

import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public class ContactSearch {
    private ContactSearchResult mSearchResult;
    private View mView;
    private SearchView searchView;
    private Context mContext;
    private ArrayList<Contact> allContacts = new ArrayList<>();

    public ContactSearch(ContactSearchResult searchResult, SearchView view, Context context, ArrayList<Contact> mContacts) {
        mSearchResult = searchResult;
        searchView = view;
        mContext = context;
        allContacts.addAll(mContacts);
        initSearchElement();
    }

    private void initSearchElement() {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
//                Toast.makeText(mContext, query, Toast.LENGTH_LONG).show();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                searchForFriend(newText);
                return true;
            }
        });

    }

    private void searchForFriend(String newText) {
        ArrayList<Contact> searchItem = new ArrayList<>();
        for (Contact contact : allContacts) {
            if (contact.getPhone().contains(newText) | (contact.getName().toLowerCase().contains(newText))) {
                searchItem.add(contact);
            }

            mSearchResult.getSearchResult(searchItem);
        }
        if (searchItem.size() < 1) {
            Toast.makeText(mContext, "no result found", Toast.LENGTH_SHORT).show();

        }
    }
}


