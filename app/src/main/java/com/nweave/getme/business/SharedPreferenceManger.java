package com.nweave.getme.business;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nweave.getme.model.Contact;
import com.nweave.getme.model.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPreferenceManger {
    //    private  Context mContext;

    private SharedPreferenceManger() {

    }
//    private void setContext(Context context){
//        mContext=context;
//    }

    private static SharedPreferenceManger sharedPreferenceManger = new SharedPreferenceManger();

    public static SharedPreferenceManger getSharedPreferenceMangerInstance() {
        return sharedPreferenceManger;
    }


    public void saveContactList(List<Contact> contacts, Context context, String prefNameItem) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.CONTACTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(contacts);
        editor.putString(prefNameItem, json);
        editor.apply();
    }

    public ArrayList<Contact> getDataFromSharedPreference(Context context, String prefNameItem) {
        SharedPreferences sharedPref = context.getSharedPreferences(com.nweave.getme.business.Constants.CONTACTS, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPref.getString(prefNameItem, null);
        Type type = new TypeToken<ArrayList<Contact>>() {
        }.getType();
        ArrayList<Contact> mContacts = gson.fromJson(json, type);
        return mContacts;
    }

    public void saveAccessTokenFCM(Context context, String accessToken) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.ACCESS_TOKEN_FCM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(com.nweave.getme.business.Constants.ACCESS_TOKEN_FCM, accessToken);
        editor.apply();
    }

    public String getAccessToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.ACCESS_TOKEN_FCM, Context.MODE_PRIVATE);
        String token = sharedPref.getString(com.nweave.getme.business.Constants.ACCESS_TOKEN_FCM, "");
        return token;
    }

    public void saveCurrentUser(Context context, User user) {
        SharedPreferences sharedPref = context.getSharedPreferences(com.nweave.getme.business.Constants.CONTACTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString(com.nweave.getme.business.Constants.CURRENT_USER, json);
        editor.apply();
    }

    public User getCurrentUser(Context context, String currentUserPref) {
        SharedPreferences sharedPref = context.getSharedPreferences(com.nweave.getme.business.Constants.CONTACTS, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPref.getString(currentUserPref, null);
        User currentUser = gson.fromJson(json, User.class);
        return currentUser;

    }

    public void savetime(Context context,String key, long value){
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        sharedPref.edit().putLong(key, value).apply();
    }

    public long getTime(Context context ,String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return sharedPref.getLong(key,0);
    }

    public void removeKey(Context context ,String key) {
//        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
//        sharedPref.edit().remove(key).commit();
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editts = sharedPreferences.edit();
        editts.clear().apply();
    }


    public void saveBoolean(Context context,String key, boolean value){
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        sharedPref.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(Context context ,String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(key,false);
    }

}



