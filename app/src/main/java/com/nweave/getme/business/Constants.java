package com.nweave.getme.business;

public class Constants {
    public static final String FRIENDS="friends";
    public static final String USER="users";
    public static final String REQUESTS="requests";
    public static final String REMOVED_CONTACTS="removed_contacts";
    public static final String FRIENDS_CONTACTS="friends_contacts";
    public static final String CONTACTS="contacts";
    public static final String ACCESS_TOKEN_FCM="accessToken";
    public static final String PHONE="phone";
    public static final String SENDER = "senderID";
    public static final String RECEIVER = "receiverID";
    public static final String LATITUDE="latitude";
    public static final String LONGITUDE="longitude";
    public static final String GREETING="greeting";
    public static final String NOTIFICATION_ID="id";
    public static final String REQUEST="request";
    public static final String CHAT ="chat" ;
    public static String UserPHONE="userPhone";
    public static String STATE="state";
    public static String NAME="name";
    public static String EMAIL="email";
    public static String PROFILE_PIC="profilePic";
    public static String CURRENT_USER="current_user";
    public static String USER_PHONE="userPhone";


}
