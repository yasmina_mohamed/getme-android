package com.nweave.getme.business.search;

import com.nweave.getme.model.User;

import java.util.ArrayList;

public interface UserSearchResult {
    void getSearchResult(ArrayList<User>searchItem);

}
