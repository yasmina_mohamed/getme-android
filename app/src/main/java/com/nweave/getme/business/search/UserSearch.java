package com.nweave.getme.business.search;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import com.nweave.getme.model.User;

import java.util.ArrayList;

public class UserSearch {
    private UserSearchResult mSearchResult;
//    private View mView;
    private SearchView searchView;
    private Context mContext;
    private ArrayList<User> allUsers = new ArrayList<>();

    public UserSearch(UserSearchResult searchResult, SearchView view, Context context, ArrayList<User> mContacts) {
        mSearchResult = searchResult;
        searchView = view;
        mContext = context;
        allUsers.addAll(mContacts);
        initSearchElement();
    }


    private void initSearchElement() {
//        searchView = mView.findViewById(R.id.search_view);
//        searchView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                searchView.setIconified(false);
//            }
//        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(mContext, query, Toast.LENGTH_LONG).show();
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                searchForFriend(newText);
                return true;
            }
        });

    }

    private void searchForFriend(String newText) {
        ArrayList<User> searchItem = new ArrayList<>();
        if (allUsers.size() > 0) {
            for (User user : allUsers) {
                if ((user.getUserPhone().contains(newText)) | (user.getName().toLowerCase().contains(newText))) {
                    searchItem.add(user);
                }
                mSearchResult.getSearchResult(searchItem);
            }
        }
    }


//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.search_view:
//            searchView.setIconified(false);
//            break;
//        }
//    }


}

