package com.nweave.getme.business.search;

import com.nweave.getme.model.Contact;

import java.util.ArrayList;

public interface ContactSearchResult {
    void getSearchResult(ArrayList<Contact> searchItem);
}
