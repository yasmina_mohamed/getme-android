package com.nweave.getme.chat.presenter;

public interface IChatPresenter {
    void getMessages(String roomID);
}
