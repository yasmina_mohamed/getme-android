package com.nweave.getme.chat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.chat.presenter.ChatPresenter;
import com.nweave.getme.chat.presenter.IChatPresenter;
import com.nweave.getme.model.User;
import com.nweave.getme.notification.NotificationCenter;
import com.nweave.getme.notification.NotificationListener;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;


public class ChatActivity extends AppCompatActivity implements View.OnClickListener, ChatView {
    private RecyclerView recyclerChat;
    public static final int VIEW_TYPE_USER_MESSAGE = 0;
    public static final int VIEW_TYPE_FRIEND_MESSAGE = 1;
    private ListMessageAdapter adapter;
    private String roomId;
    private ArrayList<CharSequence> idFriend;
    private Consersation consersation;
    private ImageButton btnSend;
    private EditText editWriteMessage;
    private LinearLayoutManager linearLayoutManager;
    private String friendKey;
    private IChatPresenter chatPresenter;
    private ImageButton btnAdd;
    private String accessToken;
    private String nameFriend;
    private User friend;


    @BindView(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent intentData = getIntent();
        ButterKnife.bind(this);
        //  setSupportActionBar(toolbar);
        if(intentData!=null){
            friend = intentData.getParcelableExtra("friend");
        }

        accessToken=intentData.getStringExtra("access_token");
        idFriend = intentData.getCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID);
        roomId = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID);
        nameFriend = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND);
        friendKey = intentData.getStringExtra(StaticConfig.INTENT_FRIEND_KEY);

        consersation = new Consersation();
        btnSend = (ImageButton) findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        btnAdd = (ImageButton) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
        chatPresenter = new ChatPresenter(this);
        editWriteMessage = (EditText) findViewById(R.id.editWriteMessage);
        if (idFriend != null && nameFriend != null) {
           // getSupportActionBar().setTitle(nameFriend);
            linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerChat = (RecyclerView) findViewById(R.id.recyclerChat);
            recyclerChat.setLayoutManager(linearLayoutManager);
            adapter = new ListMessageAdapter(this, consersation, friendKey);
            chatPresenter.getMessages(roomId);
            recyclerChat.setAdapter(adapter);

            final ImageView profileIv = toolbar.findViewById(R.id.profile_pic);
            TextView nameTv = toolbar.findViewById(R.id.name_tv);
            nameTv.setText(nameFriend);
            if(friend.getProfilePic()!=null) {
                Picasso.get().load(friend.getProfilePic()).placeholder(R.drawable.place_holder_profile).into(profileIv);

                //      Glide.with(ChatActivity.this).load(friend.getProfilePic()).into(profileIv);

//                final Bitmap[] my_image = new Bitmap[1];
//                StorageReference ref = FirebaseStorage.getInstance().getReference().child("images/" + friend.getUid());
//                try {
//                    final File localFile = File.createTempFile(friend.getUid(), "bmp");
//                    ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
//                        @Override
//                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                            if (friend.getUid().equals(friendKey)) {
//                                my_image[0] = BitmapFactory.decodeFile(localFile.getAbsolutePath());
//                                Picasso.get().load(String.valueOf(my_image[0])).placeholder(R.drawable.place_holder_profile).into(profileIv);
//                                return;
//                            }
//                            try {
//                                my_image[0] = BitmapFactory.decodeFile(localFile.getAbsolutePath());
//                                Picasso.get().load(String.valueOf(my_image[0])).placeholder(R.drawable.place_holder_profile).into(profileIv);
//                            }catch (Exception e){
//
//                            }
//                        }
//                    }).addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//
//                        }
//                    });
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }
        }
    }

    @OnClick(R.id.arrow_back) void pressBack() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent result = new Intent();
            result.putExtra("idFriend", idFriend.get(0));
            setResult(RESULT_OK, result);
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent result = new Intent();
        result.putExtra("idFriend", idFriend.get(0));
        setResult(RESULT_OK, result);
        this.finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSend) {
            String content = editWriteMessage.getText().toString().trim();
            if (content.length() > 0) {
                editWriteMessage.setText("");
                Message newMessage = new Message();
                newMessage.text = content;
                newMessage.idSender = FirebaseAuth.getInstance().getUid();
                newMessage.idReceiver = roomId;
                newMessage.timestamp = System.currentTimeMillis();
                JSONArray regArray = new JSONArray();
                regArray.put(accessToken);
                FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage);
                new NotificationCenter(Constants.CHAT, regArray, "Hii", nameFriend+"sent a message to you", new NotificationListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getApplicationContext(), "Hi is Sent", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure() {
                        Toast.makeText(getApplicationContext(), "Hi not Sent", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else if (view.getId() == R.id.btnAdd) {
            startGetImage();
        }
    }

    private void startGetImage() {
        if (isStoragePermissionGranted()) {
            CropImage.activity().setAspectRatio(7, 7)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }
    }

    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                Log.v("permission", "Permission is granted");
                return true;
            } else {
                Log.v("permission", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("permission", "Permission is granted");
            return true;
        }
    }

    @Override
    public void setMessage(Message message) {
        consersation.getListMessageData().add(message);
        adapter.notifyDataSetChanged();
        linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                String path = resultUri.getPath(); // “/mnt/sdcard/FileName.mp3”
                uploadImageToFirebase(Uri.fromFile(new File(path)));

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.i("error User Profile", "onActivityResult: " + error.getMessage());
            }
        }
    }

    private void uploadImageToFirebase(Uri selectedImageUri) {
        if (selectedImageUri != null) {
            String id = FirebaseAuth.getInstance().getUid();
            final StorageReference ref = FirebaseStorage.getInstance().getReference().child("images/" + id);
            UploadTask uploadFile = ref.putFile(selectedImageUri);
            uploadFile.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    // Continue with the task to get the download URL
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        String imageURL = downloadUri.toString();
                        Message newMessage = new Message();
                        newMessage.imageURL = imageURL;
                        newMessage.idSender = FirebaseAuth.getInstance().getUid();
                        newMessage.idReceiver = roomId;
                        newMessage.timestamp = System.currentTimeMillis();
                        FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage);
                    }
                }
            });

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.v("permission", "Permission: " + permissions[0] + "was " + grantResults[0]);
                startGetImage();
            }
        }
    }
}

class ListMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Consersation consersation;
    private Bitmap my_image;
    private Bitmap friend_image;
    private String friendKey;

    public ListMessageAdapter(Context context, Consersation consersation, String friendKey) {
        this.context = context;
        this.consersation = consersation;
        this.friendKey = friendKey;
        downloadImage(FirebaseAuth.getInstance().getUid());
        downloadImage(friendKey);
    }

    private void downloadImage(final String id) {
        StorageReference ref = FirebaseStorage.getInstance().getReference().child("images/" + id);
        try {
            final File localFile = File.createTempFile(id, "bmp");
            ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    if (id.equals(friendKey)) {
                        friend_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        notifyDataSetChanged();
                        return;
                    }
                    try {
                        my_image = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                        notifyDataSetChanged();
                    }catch (Exception e){

                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ChatActivity.VIEW_TYPE_FRIEND_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.rc_item_message_friend, parent, false);
            return new ItemMessageFriendHolder(view);
        } else if (viewType == ChatActivity.VIEW_TYPE_USER_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.rc_item_message_user, parent, false);
            return new ItemMessageUserHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemMessageFriendHolder) {
            if (consersation.getListMessageData().get(position).text != null) {
                ((ItemMessageFriendHolder) holder).txtContent.setText(consersation.getListMessageData().get(position).text);
                ((ItemMessageFriendHolder) holder).roundedImageView.setVisibility(View.GONE);
                ((ItemMessageFriendHolder) holder).txtContent.setVisibility(View.VISIBLE);
            } else {
              //  Picasso.get().load(consersation.getListMessageData().get(position).imageURL).placeholder(R.mipmap.ic_launcher).into(((ItemMessageFriendHolder) holder).roundedImageView);

                Glide.with(context).load(consersation.getListMessageData().get(position).imageURL)
                        .apply(RequestOptions.placeholderOf(R.drawable.spinner))
                        .into(((ItemMessageFriendHolder) holder).roundedImageView);

                ((ItemMessageFriendHolder) holder).roundedImageView.setVisibility(View.VISIBLE);
                ((ItemMessageFriendHolder) holder).txtContent.setVisibility(View.GONE);
            }
            if (friend_image != null) {
                ((ItemMessageFriendHolder) holder).avata.setImageBitmap(friend_image);
            }
        } else if (holder instanceof ItemMessageUserHolder) {
            if (consersation.getListMessageData().get(position).text != null) {
                ((ItemMessageUserHolder) holder).txtContent.setText(consersation.getListMessageData().get(position).text);
                ((ItemMessageUserHolder) holder).roundedImageView.setVisibility(View.GONE);
                ((ItemMessageUserHolder) holder).txtContent.setVisibility(View.VISIBLE);
            } else {
               // Picasso.get().load(consersation.getListMessageData().get(position).imageURL).placeholder(R.mipmap.ic_launcher).into(((ItemMessageUserHolder) holder).roundedImageView);

                Glide.with(context).load(consersation.getListMessageData().get(position).imageURL)
                        .apply(RequestOptions.placeholderOf(R.drawable.spinner))
                        .into(((ItemMessageUserHolder) holder).roundedImageView);
                ((ItemMessageUserHolder) holder).roundedImageView.setVisibility(View.VISIBLE);
                ((ItemMessageUserHolder) holder).txtContent.setVisibility(View.GONE);
            }
            if (my_image != null) {
                ((ItemMessageUserHolder) holder).avata.setImageBitmap(my_image);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return consersation.getListMessageData().get(position).idSender.equals(FirebaseAuth.getInstance().getUid()) ? ChatActivity.VIEW_TYPE_USER_MESSAGE : ChatActivity.VIEW_TYPE_FRIEND_MESSAGE;
    }

    @Override
    public int getItemCount() {
        return consersation.getListMessageData().size();
    }
}

class ItemMessageUserHolder extends RecyclerView.ViewHolder {
    public TextView txtContent;
    public CircleImageView avata;
    public RoundedImageView roundedImageView;

    public ItemMessageUserHolder(View itemView) {
        super(itemView);
        txtContent = (TextView) itemView.findViewById(R.id.textContentUser);
        avata = (CircleImageView) itemView.findViewById(R.id.imageView2);
        roundedImageView = (RoundedImageView) itemView.findViewById(R.id.image);
    }
}

class ItemMessageFriendHolder extends RecyclerView.ViewHolder {
    public TextView txtContent;
    public CircleImageView avata;
    public RoundedImageView roundedImageView;

    public ItemMessageFriendHolder(View itemView) {
        super(itemView);
        txtContent = (TextView) itemView.findViewById(R.id.textContentFriend);
        avata = (CircleImageView) itemView.findViewById(R.id.imageView3);
        roundedImageView = (RoundedImageView) itemView.findViewById(R.id.image);
    }
}
