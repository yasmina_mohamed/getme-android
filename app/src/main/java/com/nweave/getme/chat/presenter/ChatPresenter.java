package com.nweave.getme.chat.presenter;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.nweave.getme.chat.ChatView;
import com.nweave.getme.chat.Message;

import java.util.HashMap;

public class ChatPresenter implements IChatPresenter {
    private ChatView chatView;

    public ChatPresenter(ChatView chatView) {
        this.chatView = chatView;
    }

    @Override
    public void getMessages(String roomID) {
        FirebaseDatabase.getInstance().getReference().child("message/" + roomID).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                    Message newMessage = new Message();
                    newMessage.idSender = (String) mapMessage.get("idSender");
                    newMessage.idReceiver = (String) mapMessage.get("idReceiver");
                    newMessage.timestamp = (long) mapMessage.get("timestamp");
                    if(mapMessage.containsKey("imageURL")){
                        newMessage.imageURL = (String) mapMessage.get("imageURL");
                    }
                    if(mapMessage.containsKey("text")){
                        newMessage.text = (String) mapMessage.get("text");
                    }
                    if (chatView != null) {
                        chatView.setMessage(newMessage);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
