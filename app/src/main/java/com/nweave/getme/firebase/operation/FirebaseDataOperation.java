package com.nweave.getme.firebase.operation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.contacts.adapter.FriendDuplicationChecker;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.home.presenter.HomePresenterInterface;
import com.nweave.getme.model.Contact;
import com.nweave.getme.model.RequestModel;
import com.nweave.getme.model.User;

import java.util.ArrayList;

public class FirebaseDataOperation {
    private FireBase fireBase;
    private HomePresenterInterface mHomePresenterInterface;
    private FriendDuplicationChecker mFriendDuplicationChecker;
    private Boolean friendExistance = false;

    public FirebaseDataOperation() {
        initFireBase();
    }

    public FirebaseDataOperation(HomePresenterInterface homePresenterInterface) {
        this();
        mHomePresenterInterface = homePresenterInterface;
        getFriends();
//        getLastAddedFriends();
    }

    public FirebaseDataOperation(FriendDuplicationChecker friendDuplicationChecker) {
        this();
        mFriendDuplicationChecker = friendDuplicationChecker;
    }

    private void initFireBase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
    }

    private DatabaseReference myFriendsRef;
    private ValueEventListener listener;

    private void getFriends() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            if (FirebaseAuth.getInstance().getCurrentUser().getUid() != null) {
                myFriendsRef = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constants.FRIENDS);
                listener = new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<String> friends = new ArrayList<>();
                        for (DataSnapshot contactDataSnapshot : dataSnapshot.getChildren()) {
                            String c = contactDataSnapshot.getKey();
                            friends.add(c);
                            Log.d("Friendname", "onDataChange: " + c);
                        }
                        mHomePresenterInterface.getFriends(friends);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                };
                myFriendsRef.addValueEventListener(listener);
            }
        }
    }


//    public void destroyListener() {
//        myFriendsRef.removeEventListener(listener);
//    }


    public void checkFriendDuplication(final Contact contact) {
        DatabaseReference myFriendsRef = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constants.FRIENDS);
        myFriendsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot contactDataSnapshot : dataSnapshot.getChildren()) {
                    Contact c = contactDataSnapshot.getValue(Contact.class);
                    if (c != null && c.getName().equals(contact.getName())) {
                        friendExistance = true;
                        break;
                    }
                }
                mFriendDuplicationChecker.getFriendDuplicationResult(contact, friendExistance);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.i("test", "onCancelled: " + databaseError.getMessage());

            }
        });
    }


    public void addToFirebase(Contact contact) {
        FireBase fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        DatabaseReference user = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        DatabaseReference pushedKey = user.child(Constants.FRIENDS).push();
        pushedKey.setValue(contact);
    }

    public void addInviteNodeToFirebase(String key) {
        FireBase fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        DatabaseReference pushedKey = fireBase.getRequestRef().push();

        RequestModel requestModel = new RequestModel();
        requestModel.setReceiverID(key);
        requestModel.setSenderID(FirebaseAuth.getInstance().getCurrentUser().getUid());
        requestModel.setState(false);
        requestModel.setKey(pushedKey.getKey());
        pushedKey.setValue(requestModel);
    }

    public void updateUserAccessToken(Context context) {
        User user = new User();
        user.setAccessToken(SharedPreferenceManger.getSharedPreferenceMangerInstance().getAccessToken(context));


        fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constants.ACCESS_TOKEN_FCM)
                .setValue(user.getAccessToken());
    }
}
