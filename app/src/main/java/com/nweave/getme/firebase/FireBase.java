package com.nweave.getme.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nweave.getme.business.Constants;

public class FireBase {

    private DatabaseReference friendsRef;
    private DatabaseReference userRef;
    private DatabaseReference requestRef;
    private DatabaseReference greetingRef;

    public DatabaseReference getLatitudeRef() {
        return latitudeRef;
    }

    public void setLatitudeRef(DatabaseReference latitudeRef) {
        this.latitudeRef = latitudeRef;
    }

    public DatabaseReference getLongitudeRef() {
        return longitudeRef;
    }

    public void setLongitudeRef(DatabaseReference longitudeRef) {
        this.longitudeRef = longitudeRef;
    }

    private DatabaseReference latitudeRef;
    private DatabaseReference longitudeRef;
    private DatabaseReference ref;
    private String uid;

    private FireBase() {
    }

    private static FireBase fireBase = new FireBase();

    public static FireBase getFireBaseInstance() {
        return fireBase;
    }

    public void initFirebaseDataBase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        friendsRef = database.getReference(Constants.FRIENDS);
        userRef = database.getReference(Constants.USER);
        requestRef = database.getReference(Constants.REQUESTS);
        latitudeRef = userRef.child(Constants.LATITUDE);
        longitudeRef = userRef.child(Constants.LONGITUDE);
        greetingRef=database.getReference(Constants.GREETING);
    }


    public DatabaseReference getFriendsRef() {
        return friendsRef;
    }

    public DatabaseReference getUserRef() {
        return userRef;
    }

    public DatabaseReference getRequestRef() {
        return requestRef;
    }

    public DatabaseReference getGreetingRef() {
        return greetingRef;
    }

    public void setGreetingRef(DatabaseReference greetingRef) {
        this.greetingRef = greetingRef;
    }
}
