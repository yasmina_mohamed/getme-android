package com.nweave.getme.home.presenter;

import com.nweave.getme.home.view.FirebaseUserViewInterface;
import com.nweave.getme.model.Contact;
import com.nweave.getme.model.User;

import java.util.ArrayList;

public class FirebaseUserPresenterImpl implements FirebaseUserPresenterInterface{
    private ArrayList<Contact>mContacts=new ArrayList<>();
    private ArrayList<User>mFriends=new ArrayList<>();
    private FirebaseUserViewInterface mFirebaseUserViewInterface;

    public FirebaseUserPresenterImpl(ArrayList<Contact>contacts, ArrayList<User>friends, FirebaseUserViewInterface firebaseUserViewInterface) {
    mContacts.addAll(contacts);
    mFriends.addAll(friends);
    mFirebaseUserViewInterface=firebaseUserViewInterface;
    initFirebaseUsers();
    }

    private void initFirebaseUsers() {
       new FirebaseUsers(mContacts,mFriends,this);
    }

    @Override
    public void getFirebaseUsers(ArrayList<User> firebaseUsers) {
        mFirebaseUserViewInterface.showFirebaseUsers(firebaseUsers);
    }
}
