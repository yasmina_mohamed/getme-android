package com.nweave.getme.home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.firebase.operation.FirebaseDataOperation;
import com.nweave.getme.model.RequestModel;
import com.nweave.getme.model.User;
import com.nweave.getme.notification.NotificationCenter;
import com.nweave.getme.notification.NotificationListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<User> mFirebaseUsers;
    private Context mContext;
    private List<String> invited;
    private boolean isTextViewClicked = false;


    public UsersAdapter(Context context) {
        mFirebaseUsers = new ArrayList<>();
//        mFirebaseUsers.addAll(firebaseUsers);
        mContext = context;
        getInvites();
    }

    public void addUsers(ArrayList<User> users) {
        mFirebaseUsers.clear();
        mFirebaseUsers.addAll(users);
        notifyDataSetChanged();
    }


    private void getInvites() {
        FireBase fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        Query q = fireBase.getRequestRef().orderByChild(Constants.SENDER).equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                invited = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    RequestModel request = d.getValue(RequestModel.class);
                    invited.add(request.getReceiverID());
                }
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @NonNull
    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final UsersAdapter.ViewHolder holder, int position) {
        if (mFirebaseUsers.get(position).getProfilePic() != null && !mFirebaseUsers.get(position).getProfilePic().isEmpty()) {
            Picasso.get().load(mFirebaseUsers.get(position).getProfilePic()).placeholder(R.drawable.place_holder_profile).into(holder.firebaseUserImage);
        } else {
            holder.firebaseUserImage.setImageResource(R.drawable.place_holder_profile);
        }
        holder.addIcone.setTag(mFirebaseUsers.get(position));
        holder.addIcone.setOnClickListener(this);

        if (checkInvite(mFirebaseUsers.get(position).getUid())) {
            holder.addIcone.setVisibility(View.INVISIBLE);
            holder.doneIcone.setVisibility(View.VISIBLE);
        } else {
            holder.addIcone.setVisibility(View.VISIBLE);
            holder.doneIcone.setVisibility(View.INVISIBLE);
        }
//check if firebase user has name or not
        if (mFirebaseUsers.get(position).getName() != null && !mFirebaseUsers.get(position).getName().isEmpty()) {
            holder.firebaseUserName.setMaxLines(1);
            holder.firebaseUserName.setText(mFirebaseUsers.get(position).getName());
        } else {
            holder.firebaseUserName.setMaxLines(1);
            holder.firebaseUserName.setText(mFirebaseUsers.get(position).getUserPhone());
        }
        holder.firebaseUserName.setOnClickListener(this);

    }

    private boolean checkInvite(String key) {
        if (invited == null || invited.isEmpty()) {
            return false;
        }
        for (String userkey : invited) {
            if (userkey.equals(key)) {
                return true;
            }
        }
        return false;
    }

    private void getReceiverRequestID(User user) {
        FireBase fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        final FirebaseDataOperation firebaseDataOperation = new FirebaseDataOperation();
        firebaseDataOperation.addInviteNodeToFirebase(user.getUid());
        JSONArray regArray = new JSONArray();
        if(user.getAccessToken()!=null && !user.getAccessToken().isEmpty()){
            regArray.put(user.getAccessToken());
        }
        new NotificationCenter(Constants.REQUEST, regArray, "You have an invitation", "Please Check your requests", new NotificationListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(mContext, "Request Sent", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure() {
                Toast.makeText(mContext, "Failed Sending Request", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return mFirebaseUsers.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_icon:
                User user = (User) v.getTag();
                getReceiverRequestID(user);
            case R.id.firebase_user_Name:
                if (v instanceof TextView) {
                    TextView textView = (TextView) v;
                    expandOrCollabseTextView(textView);
                }


        }
    }

    private void expandOrCollabseTextView(TextView holder) {
        if (isTextViewClicked) {
            holder.setMaxLines(1);
            isTextViewClicked = false;
        } else {
            holder.setMaxLines(Integer.MAX_VALUE);
            isTextViewClicked = true;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView firebaseUserImage;
        public ImageView addIcone;
        public ImageView doneIcone;
        public TextView firebaseUserName;

        public ViewHolder(View itemView) {
            super(itemView);
            firebaseUserImage = itemView.findViewById(R.id.firebase_user_image);
            addIcone = itemView.findViewById(R.id.add_icon);
            doneIcone = itemView.findViewById(R.id.done_icon);
            firebaseUserName = itemView.findViewById(R.id.firebase_user_Name);
        }
    }
}
