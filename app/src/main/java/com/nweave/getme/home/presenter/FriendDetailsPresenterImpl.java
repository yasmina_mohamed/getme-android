package com.nweave.getme.home.presenter;

import com.nweave.getme.home.view.FriendsDetailsViewInterface;
import com.nweave.getme.model.User;

import java.util.ArrayList;

public class FriendDetailsPresenterImpl implements FriendDetailsPresenterInterface {
    ArrayList<String> mFriendsIds=new ArrayList<>();
    FriendsDetailsViewInterface mFriendsDetailsViewInterface;

    public FriendDetailsPresenterImpl(ArrayList<String>friendsIds, FriendsDetailsViewInterface friendsDetailsViewInterface) {
        mFriendsIds.addAll(friendsIds);
        mFriendsDetailsViewInterface=friendsDetailsViewInterface;
        initFriendsList();
    }

    private void initFriendsList() {
     new FriendsDetails(mFriendsIds,this);
    }

    @Override
    public void getFriendsList(ArrayList<User> friends) {
        mFriendsDetailsViewInterface.getFriendsDetailsList(friends);

    }

    @Override
    public void onDestroy() {
        mFriendsDetailsViewInterface=null;
    }
}
