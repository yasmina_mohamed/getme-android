package com.nweave.getme.home;

import com.nweave.getme.model.User;

public interface FriendSelectedListener {
    void friendSelected(User friend);
}
