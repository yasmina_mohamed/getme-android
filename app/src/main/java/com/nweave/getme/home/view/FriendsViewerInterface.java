package com.nweave.getme.home.view;

import java.util.ArrayList;

public interface FriendsViewerInterface  {
    void showFriends(ArrayList<String>friends);
}
