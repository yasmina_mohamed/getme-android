package com.nweave.getme.home.view;

import com.nweave.getme.model.User;

import java.util.ArrayList;

public interface FirebaseUserViewInterface {
    void showFirebaseUsers(ArrayList<User>firebaseUsers);
}
