package com.nweave.getme.home.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nweave.getme.R;
import com.nweave.getme.base.BaseActivity;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.business.search.UserSearch;
import com.nweave.getme.business.search.UserSearchResult;
import com.nweave.getme.chat.ChatActivity;
import com.nweave.getme.chat.StaticConfig;
import com.nweave.getme.contacts.presenter.ContactsPresenterImpl;
import com.nweave.getme.contacts.view.ContactsViewerInterface;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.home.FriendSelectedListener;
import com.nweave.getme.home.adapter.FriendsAdapter;
import com.nweave.getme.home.adapter.UsersAdapter;
import com.nweave.getme.home.presenter.FirebaseUserPresenterImpl;
import com.nweave.getme.home.presenter.FriendDetailsPresenterImpl;
import com.nweave.getme.home.presenter.FriendDetailsPresenterInterface;
import com.nweave.getme.home.presenter.HomePresenterImpl;
import com.nweave.getme.home.presenter.HomePresenterInterface;
import com.nweave.getme.map.MapsActivity;
import com.nweave.getme.model.Contact;
import com.nweave.getme.model.User;
import com.nweave.getme.network.InternetConnection;
import com.nweave.getme.notification.NotificationCenter;
import com.nweave.getme.notification.NotificationListener;

import net.hockeyapp.android.UpdateManager;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class HomeActivity extends BaseActivity implements
        FriendsViewerInterface, NavigationView.OnNavigationItemSelectedListener, UserSearchResult, ContactsViewerInterface
        , View.OnTouchListener, OnGetDataListener, FriendsDetailsViewInterface, FirebaseUserViewInterface {
    private ArrayList<String> mFriendsID;
    private FriendsAdapter adapter;
    private View v;
    private FusedLocationProviderClient mFusedLocationClient;
    private FireBase fireBase;
    private User mCurrentUser;
    private DatabaseReference currentUserRef;
    private ArrayList<User> mFirebaseUsers;
    private int counter = 0;
    private int friendsCounter = 0;
    private ArrayList<Contact> mContacts;
    private RecyclerView userRecyclerView;
    private boolean firstAPIFinshed = false;
    private TextView noFriendsTextView;
    private TextView noSuggestedTextView;
    private RecyclerView friendsRecyclerView;
    private SearchView searchView;
    private boolean permissionIsGranted = false;
    private View constraintLayout;
    private UsersAdapter usersAdapter;
    private View bottomSheet;
    private BottomSheetBehavior<View> mBottomSheetBehavior;
    private ArrayList<String> selectedFriendsToShowOnMap = new ArrayList<>();
    //    private HashMap<String, User> friends;
    private ArrayList<User> mFriendsList = new ArrayList<>();
    private View viewTop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initElements();
        v = getLayoutInflater().inflate(R.layout.main_home, activityContainer);
        constraintLayout = findViewById(R.id.home_activity_layout);
        viewTop=findViewById(R.id.suggest_friend_layout);
//        constraintLayout.setOnTouchListener(this);
        initHomeActivityElements();
        initFirebase();
        initFriendsRecyclerView();
        initUserReyclerview();
        showProgressDialog();
        checkMultiPermissions();
        checkForUpdates();
        checkScreenSize();
    }

    private void checkScreenSize() {
        constraintLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = constraintLayout.getRootView().getHeight() - constraintLayout.getHeight();

                Rect r = new Rect();
                constraintLayout.getWindowVisibleDisplayFrame(r);
                int screenHeight = constraintLayout.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    Log.d("MYActivity", "keyboard is open");
//                    userRecyclerView.setVisibility(View.GONE);
                    viewTop.setVisibility(View.GONE);

                }
                else {
                    // keyboard is closed
                    Log.d("MYActivity", "keyboard is close");
//                    userRecyclerView.setVisibility(View.VISIBLE);
                    viewTop.setVisibility(View.VISIBLE);

                }
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
        if (permissionIsGranted) {
            getCurrentUserFromSharedPreference();
            getCurrentLocation();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
        if (homePresenterInterface != null) {
            homePresenterInterface.onDestroy();
        }
        if (friendDetailsPresenterInterface != null) {
            friendDetailsPresenterInterface.onDestroy();
        }

    }


    private void checkNetwork() {
        if (InternetConnection.checkConnection(this)) {
            // Internet Available...
        } else {
            // Internet Not Available...
            Toast.makeText(this, "No Internet Available", Toast.LENGTH_LONG).show();
        }
    }

    //    ---------------------------------------init Firebase------------------------------------------
    private void initFirebase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        currentUserRef = fireBase.getUserRef().child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
    }

//    ----------------------------------------init home elements------------------------------------

    private void initHomeActivityElements() {
        mFriendsID = new ArrayList<>();
//        friends = new HashMap<>();
        mFirebaseUsers = new ArrayList<>();
        mContacts = new ArrayList<>();
        searchView = v.findViewById(R.id.search_view);
        bottomSheet = findViewById(R.id.bottom_sheet);
        LinearLayout chatBtn = bottomSheet.findViewById(R.id.chat_cell);
        LinearLayout cancelBtn = bottomSheet.findViewById(R.id.cancel_cell);
        LinearLayout greetingBtn = bottomSheet.findViewById(R.id.notification_cell);
        LinearLayout navigationBtn = bottomSheet.findViewById(R.id.map_cell);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        cancelBtn.setOnClickListener(this);
        chatBtn.setOnClickListener(this);
        greetingBtn.setOnClickListener(this);
        navigationBtn.setOnClickListener(this);

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        noFriendsTextView = v.findViewById(R.id.no_friends);
        noSuggestedTextView = v.findViewById(R.id.no_suggested_friends);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    //----------------------------------------user Recycler View------------------------------------

    private void initUserReyclerview() {
        userRecyclerView = v.findViewById(R.id.firebase_user_recyclerView);
        // userRecyclerView.setOnTouchListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        userRecyclerView.setLayoutManager(layoutManager);
        usersAdapter = new UsersAdapter(this);
        userRecyclerView.setAdapter(usersAdapter);
    }

//    ---------------------------------------init Friend Recycler view-----------------------------

    private void initFriendsRecyclerView() {
        friendsRecyclerView = v.findViewById(R.id.friends_recyclerView);
//        friendsRecyclerView.setOnTouchListener(this);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 3);
        friendsRecyclerView.setLayoutManager(linearLayoutManager);

        adapter = new FriendsAdapter(this, new FriendSelectedListener() {
            @Override
            public void friendSelected(User friend) {
                selectedFriendsToShowOnMap.clear();
                selectedFriendsToShowOnMap.add(friend.getUid());
                bottomSheet.setTag(friend);
                if (mBottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

//                Intent intent = new Intent(HomeActivity.this, MapsActivity.class);
//                intent.putExtra(getString(R.string.friends), friends);
//                startActivity(intent);
            }
        });
        friendsRecyclerView.setAdapter(adapter);
    }

    //---------------------------------startPresenterToGetContact-----------------------------------

    private void initContactPresenter() {
        new ContactsPresenterImpl(this, this);
    }
    //-------------------------------ViewContacts------------------------------------------------------------

    @Override
    public void viewContacts(ArrayList<Contact> contacts) {
        checkFirstApi();
        if (contacts.size() > 0) {
            mContacts.clear();
            mContacts.addAll(contacts);
            Log.d("test", "viewContacts: full");
        } else {
            mContacts.clear();
            setSuggestedVisibile(false);
            hideProgressDialog();
            Log.d("test", "viewContacts: empty");
        }
        onGetContactsSuccess();
//       getFriends();
    }


//    ----------------------------------------------------------------------------------------

    private void getCurrentUserFromSharedPreference() {
//        SharedPreferenceManger sharedPreferenceManger = SharedPreferenceManger.getSharedPreferenceMangerInstance();
//        if ((sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER) != null)) {
//            mCurrentUser = sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER);
//            showUserDataToNavigationViewHeader(mCurrentUser);
//            onGetCurrentUserSuccess();
//
//        } else {
        getCurrentUserFromFirebase();
//        }
    }

    private void getCurrentUserFromFirebase() {
        currentUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mCurrentUser = getCurrentUserData(dataSnapshot);
                    showUserDataToNavigationViewHeader(mCurrentUser);
                    saveCurrentUserOnSharedPreference(mCurrentUser);
                    onGetCurrentUserSuccess();
                } else {
                    Toast.makeText(HomeActivity.this, "No User Data Found!", Toast.LENGTH_SHORT).show();
                    OnFailure(Constants.CURRENT_USER);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private User getCurrentUserData(DataSnapshot dataSnapshot) {
        return dataSnapshot.getValue(User.class);
    }

    private void saveCurrentUserOnSharedPreference(User user) {
        SharedPreferenceManger.getSharedPreferenceMangerInstance().saveCurrentUser(HomeActivity.this, user);
    }


//------------------------------get friends------------------------------------------------------

    private HomePresenterInterface homePresenterInterface;

    private void getFriends() {
        homePresenterInterface = new HomePresenterImpl(this);
    }

//    -------------------------show friends------------------------------------------------------

    @Override
    public void showFriends(ArrayList<String> friends) {
        checkFirstApi();
        mFriendsID.clear();
        mFriendsID.addAll(friends);
        setFriendsIdToBaseActivity(friends);

        if (mFriendsID.size() > 0) {
            saveCurrentUserFriendsIds();
            initFriendsDetailsPresenter();
        } else {
            setFriendsVisibile(false);
            mCurrentUser.setFriends(new HashMap<String, String>());
            saveUpdatedDataToSharedPreference();
            initFirebaseUserPresenter(new ArrayList<User>(), mContacts);
//            filterContactAndGetFirebaseUser();
        }
    }

    private void checkFirstApi() {
        if (firstAPIFinshed) {
            hideProgressDialog();
            return;
        }
        firstAPIFinshed = true;
    }

    private void saveCurrentUserFriendsIds() {
        setFriendsVisibile(true);
        addFriendListToCurrentUser();
//      clearFriendArraylist();
//      loopForFriendsIdAndGetFriendsDetails();
    }

    private void addFriendListToCurrentUser() {
        HashMap<String, String> friendList = new HashMap<>();
        for (String id : mFriendsID) {
            friendList.put(id, id);
            mCurrentUser.setFriends(friendList);
            saveUpdatedDataToSharedPreference();
        }
    }

    private FriendDetailsPresenterInterface friendDetailsPresenterInterface;

    private void initFriendsDetailsPresenter() {
        friendDetailsPresenterInterface = new FriendDetailsPresenterImpl(mFriendsID, this);
    }

    @Override
    public void getFriendsDetailsList(ArrayList<User> friends) {
//        ArrayList<User> mFriendsList = new ArrayList<>();
        mFriendsList.clear();
        mFriendsList.addAll(friends);
        addFriendListToAdapterAndInitFriendSearch(mFriendsList);
        initFirebaseUserPresenter(mFriendsList, mContacts);
//        filterFirebaseUserList();

    }

    private void addFriendListToAdapterAndInitFriendSearch(ArrayList<User> mFriendsList) {
        if (mFriendsList != null && !mFriendsList.isEmpty()) {
            addFriendsToAdapter(mFriendsList);
            initSearch(mFriendsList);
        } else {
            setFriendsVisibile(false);
        }
    }

    private void addFriendsToAdapter(ArrayList<User> mFriendsList) {
        if (mFriendsList.size() > 0) {
            setFriendsVisibile(true);
        } else {
            setFriendsVisibile(false);
        }

        adapter.addFriends(mFriendsList);
    }

    private void initFirebaseUserPresenter(ArrayList<User> mFriendsList, ArrayList<Contact> mContacts) {
        new FirebaseUserPresenterImpl(mContacts, mFriendsList, this);
    }

    @Override
    public void showFirebaseUsers(ArrayList<User> firebaseUsers) {
        if (firebaseUsers.size() > 0) {
            setSuggestedVisibile(true);
        } else {
            setSuggestedVisibile(false);
        }
        mFirebaseUsers.clear();
        mFirebaseUsers.addAll(firebaseUsers);
        usersAdapter.addUsers(mFirebaseUsers);
    }


    //------------------------------------init search-------------------------------------------
    private void initSearch(ArrayList<User> mFriendsList) {
        new UserSearch(this, searchView, this, new ArrayList<User>(mFriendsList));
    }

    //------------------------------search result--------------------------
    @Override
    public void getSearchResult(ArrayList<User> searchItem) {
        if (searchItem.isEmpty()) {
            setFriendsVisibile(false);
        } else {
            setFriendsVisibile(true);
            adapter.setFilter(searchItem);
        }
    }

    //    -----------------------------------location---------------------------------------------------------
    private void getCurrentLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            saveLocationToFireBase(location);
                            Log.d("test", "onSuccess: " + location.getLongitude());
                        }
                    }
                });
    }


    private void saveLocationToFireBase(Location location) {
        currentUserRef.child(Constants.LONGITUDE).setValue(location.getLongitude());
        currentUserRef.child(Constants.LATITUDE).setValue(location.getLatitude());
    }


    //    ---------------------------------------check permission-------------------------------------


    private void checkMultiPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Log.d("test", "onPermissionsChecked: ");
                            permissionIsGranted = true;


                        } else {
                            hideProgressDialog();
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hideKeyboard(v);
        return true;
    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (in != null) {
            in.hideSoftInputFromWindow(view.getWindowToken(), 0);
            searchView.clearFocus();
        }
    }

    private void setFriendsVisibile(boolean isVisible) {
        if (isVisible) {
            friendsRecyclerView.setVisibility(View.VISIBLE);
            noFriendsTextView.setVisibility(View.GONE);
        } else {
            friendsRecyclerView.setVisibility(View.INVISIBLE);
            noFriendsTextView.setVisibility(View.VISIBLE);
        }
    }

    private void setSuggestedVisibile(boolean isVisible) {
        if (isVisible) {
            userRecyclerView.setVisibility(View.VISIBLE);
            noSuggestedTextView.setVisibility(View.GONE);
        } else {
            userRecyclerView.setVisibility(View.INVISIBLE);
            noSuggestedTextView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onGetContactsSuccess() {
//        clearFriendArraylist();
        getFriends();
    }

    @Override
    public void onGetCurrentUserSuccess() {
        initContactPresenter();
    }

    @Override
    public void OnFailure(String id) {

    }

    private void saveUpdatedDataToSharedPreference() {
        SharedPreferenceManger.getSharedPreferenceMangerInstance().saveCurrentUser(HomeActivity.this, mCurrentUser);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }


    //<<<<<<< Updated upstream
    private void goToChatScreen(User friend) {
        Intent intent = new Intent(HomeActivity.this, ChatActivity.class);
        intent.putExtra("friend",friend);
        intent.putExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND, friend.getName());
        intent.putExtra("access_token",friend.getAccessToken());
        ArrayList<CharSequence> idFriend = new ArrayList<CharSequence>();
        idFriend.add(friend.getUid());
        intent.putCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID, idFriend);
        String idRoom = friend.getUid().compareTo(FirebaseAuth.getInstance().getUid()) > 0 ? (FirebaseAuth.getInstance().getUid() + friend.getUid()).hashCode() + "" : "" + (friend.getUid() + FirebaseAuth.getInstance().getUid()).hashCode();
        intent.putExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID, idRoom);
        intent.putExtra(StaticConfig.INTENT_FRIEND_KEY, friend.getUid());
        startActivity(intent);
    }

    //=======
    @Override
    public void onClick(View v) {
        User friend = (User) bottomSheet.getTag();
        switch (v.getId()) {
            case R.id.chat_cell:
//                Toast.makeText(this, "chat", Toast.LENGTH_SHORT).show();
                goToChatScreen(friend);
                break;
            case R.id.map_cell:
//                Toast.makeText(this, "map", Toast.LENGTH_SHORT).show();
                Boolean mapExist = isGoogleMapsInstalled();
                if (mapExist) {
                    navigateToGoogleMap(friend);
                } else {
                    Intent intent = new Intent(HomeActivity.this, MapsActivity.class);
                    intent.putExtra(getString(R.string.friends), selectedFriendsToShowOnMap);
                    startActivity(intent);
                }

                break;
            case R.id.cancel_cell:
//                Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            case R.id.notification_cell:

                JSONArray regArray = new JSONArray();
                if(friend.getAccessToken()!=null && !friend.getAccessToken().isEmpty()){
                    regArray.put(friend.getAccessToken());
                }

                new NotificationCenter(Constants.GREETING, regArray, "Hii this"+friend.getName(), "Let's Meet Now", new NotificationListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(HomeActivity.this, "Hi is Sent", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure() {
                        Toast.makeText(HomeActivity.this, "Hi not Sent", Toast.LENGTH_SHORT).show();
                    }
                });

////                Toast.makeText(this, "greeting", Toast.LENGTH_SHORT).show();
//                DatabaseReference push = fireBase.getGreetingRef().push();
//                String pushedKey = push.getKey();
//                push.child("key").setValue(pushedKey);
//                push.child(Constants.SENDER).setValue(mCurrentUser.getUid());
//                push.child(Constants.RECEIVER).setValue(friend.getUid());
//                Toast.makeText(this, "Hi is sent", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    public boolean isGoogleMapsInstalled() {
        Boolean isMapExist = false;
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            isMapExist = info.enabled;
            return isMapExist;
        } catch (PackageManager.NameNotFoundException e) {
            return isMapExist;
        }
    }

    private void navigateToGoogleMap(User friend) {
        double destinationLatitude = friend.getLatitude();
        double destinationLongitude = friend.getLongitude();
        String url = "http://maps.google.com/maps?f=d&daddr=" + destinationLatitude + "," + destinationLongitude + "&dirflg=d&layer=t";
        Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
        mapIntent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }


}

