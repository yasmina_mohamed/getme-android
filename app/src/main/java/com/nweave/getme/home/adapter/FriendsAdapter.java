package com.nweave.getme.home.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nweave.getme.R;
import com.nweave.getme.home.FriendSelectedListener;
import com.nweave.getme.map.MapsActivity;
import com.nweave.getme.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> implements View.OnClickListener {

    private Context mContext;
    private ArrayList<User> mFriends = new ArrayList<>();
    private FriendSelectedListener friendSelectedListener;

    public FriendsAdapter(Context context,FriendSelectedListener friendSelectedListener) {
        mContext = context;
        mFriends.clear();
        this.friendSelectedListener = friendSelectedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friends_list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        setFriendName(holder, position);
        setFriendImg(holder, position);
    }

    private void setFriendName(ViewHolder holder, int position) {
        if (mFriends.get(position).getName() == null || mFriends.get(position).getName().isEmpty()) {
            holder.textView.setText(mFriends.get(position).getUserPhone());
        } else {
            holder.textView.setText(mFriends.get(position).getName());
        }
    }

    private void setFriendImg(ViewHolder holder, int position) {
        if (mFriends.get(position).getProfilePic() != null && !mFriends.get(position).getProfilePic().isEmpty()){
            Picasso.get().load(mFriends.get(position).getProfilePic()).placeholder(R.drawable.place_holder_profile).into(holder.imageView);
        }else {
            holder.imageView.setImageResource(R.drawable.place_holder_profile);
        }
        holder.imageView.setOnClickListener(this);
        holder.imageView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }

    @Override
    public void onClick(View v) {
        int pos = (int) v.getTag();
        User user = mFriends.get(pos);
        friendSelectedListener.friendSelected(user);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.friend_img_imageview);
            textView = itemView.findViewById(R.id.friend_name_txtview);
        }
    }

    public void setFilter(ArrayList<User> friend) {
        mFriends = new ArrayList<>();
        mFriends.addAll(friend);
        notifyDataSetChanged();

    }

    public void addFriends(ArrayList<User> friends) {
        mFriends.clear();
        mFriends.addAll(friends);
        for (User user : friends){
            Log.d("Friendname",user.getName());
        }
        notifyDataSetChanged();
    }
}
