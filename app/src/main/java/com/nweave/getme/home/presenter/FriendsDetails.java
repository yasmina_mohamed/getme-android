package com.nweave.getme.home.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.model.User;

import java.util.ArrayList;

public class FriendsDetails {
    private int friendsCounter;
    private ArrayList<String> mFriendsID = new ArrayList<>();
    private ArrayList<User> friends = new ArrayList<>();
    private FireBase fireBase;
    private FriendDetailsPresenterImpl mFriendDetailsPresenter;

    public FriendsDetails(ArrayList<String> mFriendsIds, FriendDetailsPresenterImpl friendDetailsPresenter) {
        initFirebase();
        mFriendsID.addAll(mFriendsIds);
        mFriendDetailsPresenter=friendDetailsPresenter;
        loopForFriendsIdAndGetFriendsDetails();
    }

    private void initFirebase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
//        currentUserRef = fireBase.getUserRef().child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());

    }


    private void loopForFriendsIdAndGetFriendsDetails() {
        friendsCounter = 0;
        friends.clear();
        for (String id : mFriendsID) {
            Log.d("Friendname","FriendName key is " + id);
            getFriendsDetailsFromFirebase(id);
//        getFriendsDetailsFromFirebase(mFriendsID.get(0));
        }
    }

    private void getFriendsDetailsFromFirebase(String id) {
        fireBase.getUserRef().child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    Log.d("Friendname", "onDataChangesssss: " + user.getUid());
                      friends.add(user);
//                    friends.put(user.getUid(), user);
                } else {
                    Log.d("Friendname", "FriendName not exist " + dataSnapshot.getKey());
                }
                friendsCounter++;
                if (friendsCounter == mFriendsID.size()) {
                    Log.d("Friendname", "FriendName counters " + friendsCounter);
                    Log.d("Friendname", "FriendName maps " + friends.size());
                    mFriendDetailsPresenter.getFriendsList(friends);
                }
//                    addFriendListToAdapterAndInitFriendSearch();
//                    filterFirebaseUserList();
//                } else if (friendsCounter < mFriendsID.size()) {
//                    Log.d("Friendname", "FriendName counters " + friendsCounter);
//                    getFriendsDetailsFromFirebase(mFriendsID.get(friendsCounter));
//                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
