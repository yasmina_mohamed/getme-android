package com.nweave.getme.home.presenter;

import com.nweave.getme.model.User;

import java.util.ArrayList;

public interface FirebaseUserPresenterInterface {
    void getFirebaseUsers(ArrayList<User>firebaseUsers);
}
