package com.nweave.getme.home.presenter;

import com.nweave.getme.model.User;

import java.util.ArrayList;

public interface FriendDetailsPresenterInterface {

    void getFriendsList(ArrayList<User> friends);
    void onDestroy();
}
