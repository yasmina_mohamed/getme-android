package com.nweave.getme.home.presenter;

import java.util.ArrayList;

public interface HomePresenterInterface {
    void getFriends(ArrayList<String>friends);
    void onDestroy();
}
