package com.nweave.getme.home.view;

public interface OnGetDataListener {
    void onGetContactsSuccess();
    void onGetCurrentUserSuccess();
    void OnFailure(String id);
}
