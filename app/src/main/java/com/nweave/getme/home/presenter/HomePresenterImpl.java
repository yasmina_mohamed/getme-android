package com.nweave.getme.home.presenter;

import com.nweave.getme.firebase.operation.FirebaseDataOperation;
import com.nweave.getme.home.view.FriendsViewerInterface;

import java.util.ArrayList;

public class HomePresenterImpl implements HomePresenterInterface {
    private FriendsViewerInterface mFriendsViewerInterface;

    public HomePresenterImpl(FriendsViewerInterface friendsViewerInterface) {
        mFriendsViewerInterface = friendsViewerInterface;
        initFriendsGetter();
    }


    private void initFriendsGetter() {
        new FirebaseDataOperation(this);
    }

    @Override
    public void getFriends(ArrayList<String> friends) {
        if (mFriendsViewerInterface != null) {
            mFriendsViewerInterface.showFriends(friends);
        }
    }

    @Override
    public void onDestroy() {
        mFriendsViewerInterface = null;
    }
}
