package com.nweave.getme.home.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nweave.getme.business.Constants;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.model.Contact;
import com.nweave.getme.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FirebaseUsers {

    private ArrayList<Contact> mContacts=new ArrayList<>();
    private ArrayList<User> mFriends=new ArrayList<>();
    private FirebaseUserPresenterImpl mFirebaseUserPresenter;
    private int counter=0;
    private ArrayList<User>firebaseUsers=new ArrayList<>();
    private FireBase fireBase;



    public FirebaseUsers(ArrayList<Contact> contacts, ArrayList<User> friends, FirebaseUserPresenterImpl firebaseUserPresenter) {
        mContacts.addAll(contacts);
        mFriends.addAll(friends);
        initFirebase();
        mFirebaseUserPresenter=firebaseUserPresenter;
        removeFriendListFromContacts();
        filterContactAndGetFirebaseUser();

    }

    private void initFirebase() {
        fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
//        currentUserRef = fireBase.getUserRef().child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
    }
    private void removeFriendListFromContacts() {
        if (mContacts == null || mContacts.isEmpty()) {
//            setSuggestedVisibile(false);
            return;
        }
        ArrayList<Contact> removedContact = new ArrayList<>();
        Iterator it = mContacts.iterator();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            String phone = contact.getPhone();
            String newPhone = phone.replace(" ", "");
//            List<User> users = new ArrayList<>(friends.values());
            for (User user : mFriends) {
                String phoneUser = user.getUserPhone();
                String newPhoneUser = phoneUser.replace(" ", "");
                if (newPhoneUser.contains(newPhone)) {
//                    it.remove();
                    removedContact.add(contact);
                }

            }

        }

        mContacts.removeAll(removedContact);

    }

    private void filterContactAndGetFirebaseUser() {
        if (mContacts == null || mContacts.isEmpty()) {
//            setSuggestedVisibile(false);
            mFirebaseUserPresenter.getFirebaseUsers(new ArrayList<User>());

            Log.d("test", "filterContactAndGetFirebaseUser: emptyContact");
            return;
        }
//        setSuggestedVisibile(true);
        Iterator it = mContacts.iterator();
        firebaseUsers.clear();
        while (it.hasNext()) {
            Contact contact = (Contact) it.next();
            String phone = contact.getPhone();
            String newPhone = phone.replace(" ", "");
            checkUserExistenceInFireBase(newPhone);
        }
    }


    private void checkUserExistenceInFireBase(String phone) {
        counter = 0;
        fireBase.getUserRef().orderByChild(Constants.USER_PHONE).equalTo("+2" + phone)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot firebaseUserDataSnapshot : dataSnapshot.getChildren()) {
                                User user = firebaseUserDataSnapshot.getValue(User.class);
                                Log.d("test", "onDataChange: " + user.getName());
                                firebaseUsers.add(user);
                                counter++;
                            }

                        } else {
                            counter++;
                            Log.d("test", "user NoT exist");
                        }
                        if (counter == mContacts.size()) {
                            firebaseUserListnerResult();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    public void firebaseUserListnerResult() {
        Map<String, User> hashMap = new HashMap<>();

        if (firebaseUsers != null && !firebaseUsers.isEmpty()) {
//            setSuggestedVisibile(true);
            for (User user : firebaseUsers) {
                String phone = user.getUserPhone().replace(" ", "");
                if (!(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber()).contains(phone)) {
                    hashMap.put(user.getUid(), user);
                }
            }
//            usersAdapter.addUsers(new ArrayList<User>(hashMap.values()));
        }
        mFirebaseUserPresenter.getFirebaseUsers(new ArrayList<User>(hashMap.values()));


//        else {
//            setSuggestedVisibile(false);
//        }
    }

}
