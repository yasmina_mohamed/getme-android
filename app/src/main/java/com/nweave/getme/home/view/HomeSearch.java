package com.nweave.getme.home.view;

import java.util.ArrayList;

public interface HomeSearch {
    void getSearchResult(ArrayList<String> searchItem);
}
