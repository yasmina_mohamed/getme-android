package com.nweave.getme.notification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.chat.ChatActivity;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.requests.RequestActivity;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private NotificationManager notifManager;
    private Intent intent = null;
    NotificationCompat.Builder builder;


    @Override
    public void onNewToken(String token) {
        SharedPreferenceManger.getSharedPreferenceMangerInstance().saveAccessTokenFCM(this, token);
        Log.d("token", "Refreshed token: " + token);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        final int NOTIFY_ID = 0; // ID of notification
        String id = this.getString(R.string.default_notification_channel_id); // default_channel_id
        String title = this.getString(R.string.default_notification_channel_title); // Default Channel
        PendingIntent pendingIntent;

        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        componentInfo.getPackageName();
        if (taskInfo.get(0).topActivity.getClassName().equals(ChatActivity.class.getName())) {
            Log.d("topActivity", "onMessageReceived: " + "don't show notification");
        } else {
            Log.d("topActivity", "onMessageReceived: " + "show notification");
            intent = initIntent(remoteMessage);
            TaskStackBuilder stackBuilder = initStackBuilder();
//        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            initNotificationManger();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = notifManager.getNotificationChannel(id);
                if (mChannel == null) {
                    mChannel = new NotificationChannel(id, title, importance);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    notifManager.createNotificationChannel(mChannel);
                }
                builder = createNotification(remoteMessage, id, pendingIntent);
            } else {
                builder = createNotification(remoteMessage, id, pendingIntent);
            }
            Notification notification = builder.build();
            notifManager.notify(NOTIFY_ID, notification);
        }

    }

    private Intent initIntent(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().get(Constants.NOTIFICATION_ID).equals(Constants.REQUEST)) {
            intent = new Intent(this, RequestActivity.class);
        }

        if (remoteMessage.getData().get(Constants.NOTIFICATION_ID).equals(Constants.GREETING)) {
            intent = new Intent(this, HomeActivity.class);
        }
        if(remoteMessage.getData().get(Constants.NOTIFICATION_ID).equals(Constants.CHAT)){
            intent = new Intent(this, HomeActivity.class);

        }

        return intent;
    }

    private TaskStackBuilder initStackBuilder() {
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        if (intent != null) {
            stackBuilder.addNextIntentWithParentStack(intent);
            stackBuilder.addParentStack(HomeActivity.class);
        }
        return stackBuilder;
    }

    private void initNotificationManger() {
        if (notifManager == null) {
            notifManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        }
    }

    private NotificationCompat.Builder createNotification(RemoteMessage remoteMessage, String id, PendingIntent pendingIntent) {
        builder = new NotificationCompat.Builder(this, id)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        return builder;
    }
}
