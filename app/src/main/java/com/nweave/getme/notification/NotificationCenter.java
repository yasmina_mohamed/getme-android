package com.nweave.getme.notification;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationCenter {

    private String id;
    private JSONArray regArray;
    private String title;
    private String message;
    private NotificationListener notificationListener;

    public NotificationCenter(String id,JSONArray regArray, String title,String message, NotificationListener notificationListener) {
        this.id = id;
        this.regArray = regArray;
        this.title = title;
        this.message = message;
        this.notificationListener = notificationListener;
        sendMessage(id,regArray,title,message,notificationListener);
    }

    static String postToFCM(String bodyString) throws IOException {
        RequestBody body = RequestBody.create(JSON, bodyString);
        String serverKey = "AAAAquoZ3P0:APA91bETj4a0yMSSyBBO5p9leR7VRWbC2fY5uKsAxThCTGUyfP0wZQcqvzQlq-r1EjAOMQKVBtbWZUB2SyUkvO6p6USF2ykCv7CaGPHlfnquJ5nneWsp9SQMkz_bJhj-p6in8uQ-aqLR";
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + serverKey)
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }


    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private static OkHttpClient mClient = new OkHttpClient();

    public static void sendMessage(final String id, final JSONArray regArray, final String title, final String message, final NotificationListener listener) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {

                    JSONObject root = new JSONObject();

//                    JSONObject notification = new JSONObject();
////                   // notification.put("id", id);
////                    notification.put("body", message);
////                    notification.put("title", title);

                    JSONObject data = new JSONObject();
                    data.put("id", id);
                    data.put("message", message);
                    data.put("body", message);
                    data.put("title", title);
                   // root.put("notification", notification);
                    root.put("data", data);
                    root.put("registration_ids", regArray);

                    String result = postToFCM(root.toString());
                    Log.d("Result", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    Log.d("message", "Message Success: " + success + "Message Failed: " + failure);
                    if (listener != null) {
                        if (success == 1) {
                            listener.onSuccess();
                        } else {
                            listener.onFailure();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("message", "Message Failed, Unknown error occurred.");
                    if (listener != null) {
                        listener.onFailure();
                    }
                }
            }
        }.execute();
    }
}
