package com.nweave.getme.notification;

public interface NotificationListener {
    void onSuccess();
    void onFailure();
}
