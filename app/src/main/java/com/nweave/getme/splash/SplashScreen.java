package com.nweave.getme.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.nweave.getme.R;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.login.view.InitActivityInterface;
import com.nweave.getme.login.view.SignInActivity;

import net.hockeyapp.android.CrashManager;

public class SplashScreen extends AppCompatActivity implements InitActivityInterface {

    private long SPLASH_TIME_OUT = 2000;
    private static final int RC_SIGN_IN = 123 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                checkCurrentUserExistence();

            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkCurrentUserExistence() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            initActivity();
        } else {
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void initActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
