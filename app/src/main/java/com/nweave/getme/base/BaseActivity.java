package com.nweave.getme.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnSuccessListener;
import com.nweave.getme.R;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.contacts.view.ContactsActivity;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.login.view.SignInActivity;
import com.nweave.getme.map.MapsActivity;
import com.nweave.getme.model.User;
import com.nweave.getme.profile.UserProfileActivity;
import com.nweave.getme.requests.RequestActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private Toolbar toolbar;
    protected FrameLayout activityContainer;
    protected View headerView;
    protected ImageView toolbarImage;
    private BaseViewBridge baseViewBridge;
    private ArrayList<String> friends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        baseViewBridge = new BaseViewBridge(this);
    }

    protected void showProgressDialog() {
        baseViewBridge.showProgressDialog();
    }

    protected void hideProgressDialog() {
        baseViewBridge.hideProgressDialog();
    }


    protected void initElements() {
        initFrameLayout();
        initToolbar();
        initDrawer();
    }

    protected void initFrameLayout() {
        activityContainer = findViewById(R.id.layout_container);
    }

    protected void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
//         toolbarImage=findViewById(R.id.toolbarimage);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    protected void initDrawer() {
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, 0, 0);
        toggle.setDrawerIndicatorEnabled(false);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
                hideKeyboard(view);
            }
        });
        toggle.setHomeAsUpIndicator(R.drawable.menu);
        NavigationView navigationView = findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
//        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (in != null) {
            in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    protected void showUserDataToNavigationViewHeader(User mCurrentUser) {
        TextView userPhone = headerView.findViewById(R.id.nav_header_user_phone);
        TextView userName = headerView.findViewById(R.id.nav_header_user_name);
        ImageView profilePic = headerView.findViewById(R.id.nav_header_profile_pic);
        if (null != mCurrentUser) {
            showUserName(userName, mCurrentUser);
            showUserPic(profilePic, mCurrentUser);
            showUserPhone(userPhone, mCurrentUser);
        }
    }

    protected void setFriendsIdToBaseActivity(ArrayList<String> mFriends) {
        friends = new ArrayList<>();
        friends.addAll(mFriends);
    }

    private void showUserName(TextView userName, User mCurrentUser) {
        if (null != mCurrentUser.getName()) {
            userName.setText(mCurrentUser.getName());
        } else {
            userName.setText("user name");
        }
    }

    private void showUserPic(ImageView profilePic, User mCurrentUser) {
        if (null != mCurrentUser.getProfilePic()) {
//            Picasso.get().load(mCurrentUser.getProfilePic()).placeholder(R.drawable.place_holder).into(profilePic);
            Picasso.get().invalidate(mCurrentUser.getProfilePic());
            Picasso.get().load(mCurrentUser.getProfilePic()).placeholder(R.drawable.place_holder).into(profilePic);
        } else {
            profilePic.setImageResource(R.drawable.place_holder);
        }
    }

    private void showUserPhone(TextView userPhone, User mCurrentUser) {
        String phone = mCurrentUser.getUserPhone();
        if (phone != null || phone.equals("")) {
            userPhone.setText(phone);
        } else {
            userPhone.setText("000000");
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        checkSelectedItem(item);
        closeDrawer();
        return true;
    }

    private void checkSelectedItem(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent i = new Intent(this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
        }

        if (id == R.id.nav_signOut) {
            signOutFromFireBase();
        }
        if (id == R.id.nav_request) {
            Intent i = new Intent(this, RequestActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
        }
        if (id == R.id.nav_profile) {
            Intent i = new Intent(this, UserProfileActivity.class);
            startActivity(i);
        }
        if (id == R.id.nav_contacts) {
            Intent i = new Intent(this, ContactsActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(i);
        }
        if (id == R.id.nav_map) {
//            ArrayList<String> friends = new ArrayList<>();
            if (friends != null && !friends.isEmpty()) {
//                List<User> users = new ArrayList<>(this.friends.values());
//                for (User friend : users) {
//                    friends.add(friend.getUid());
//                }
//            }
                Intent intent = new Intent(this, MapsActivity.class);
                intent.putExtra(getString(R.string.friends), friends);
                startActivity(intent);
            }
        }
    }

    private void signOutFromFireBase() {
        AuthUI.getInstance().signOut(this).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                SharedPreferenceManger sharedPreferenceManger = SharedPreferenceManger.getSharedPreferenceMangerInstance();
//                sharedPreferenceManger.removeKey(BaseActivity.this, Constants.CURRENT_USER);
//                sharedPreferenceManger.removeKey(BaseActivity.this, Constants.FRIENDS_CONTACTS);
//                sharedPreferenceManger.removeKey(BaseActivity.this, Constants.REMOVED_CONTACTS);
                sharedPreferenceManger.removeKey(BaseActivity.this, Constants.CONTACTS);

//                sharedPreferenceManger.removeKey(BaseActivity.this,);
                // sharedPreferenceManger.saveCurrentUser(BaseActivity.this, null);
//                        sharedPreferenceManger.saveContactList(null,getApplicationContext(),Constants.FRIENDS_CONTACTS);
//                        sharedPreferenceManger.saveContactList(null,getApplicationContext(),Constants.REMOVED_CONTACTS);
//                        sharedPreferenceManger.saveAccessTokenFCM(getApplicationContext(),null);
                Intent intent = new Intent(BaseActivity.this, SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {


    }



}
