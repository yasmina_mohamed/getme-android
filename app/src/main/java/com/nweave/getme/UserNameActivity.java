package com.nweave.getme;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.nweave.getme.business.Constants;
import com.nweave.getme.business.SharedPreferenceManger;
import com.nweave.getme.firebase.FireBase;
import com.nweave.getme.home.view.HomeActivity;
import com.nweave.getme.model.User;

public class UserNameActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText userNameEditText;
    private Button nextBtn;
    private User mCurrentUser;
    private DatabaseReference currentUserRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initElements();
        initFirebase();
        getCurrentUserFromSharedPreference();
    }

    private void initElements() {
        userNameEditText = findViewById(R.id.user_name_signeUp);
        nextBtn = findViewById(R.id.nextBtn);
        nextBtn.setOnClickListener(this);
    }

    private void initFirebase() {
        FireBase fireBase = FireBase.getFireBaseInstance();
        fireBase.initFirebaseDataBase();
        currentUserRef = fireBase.getUserRef().child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    }

    private void getCurrentUserFromSharedPreference() {
        SharedPreferenceManger sharedPreferenceManger = SharedPreferenceManger.getSharedPreferenceMangerInstance();
        if ((sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER) != null)) {
            mCurrentUser = sharedPreferenceManger.getCurrentUser(this, Constants.CURRENT_USER);
            mCurrentUser.setName(userNameEditText.getText().toString());

        }
    }


    @Override
    public void onClick(View v) {
        if (userNameEditText.getText().toString().equals("") || userNameEditText.getText().toString().isEmpty()) {
            userNameEditText.setError("Name is required!");
        } else {
            String userName = userNameEditText.getText().toString();
            mCurrentUser.setName(userName);
            saveCurrentUserOnSharedPreference();
            saveUserFirebase();
//            Intent intent=new Intent(this,HomeActivity.class);
//            startActivity(intent);
//            finish();

        }
    }

    private void saveCurrentUserOnSharedPreference() {
        SharedPreferenceManger.getSharedPreferenceMangerInstance().saveCurrentUser(this, mCurrentUser);
    }

    private void saveUserFirebase() {
        currentUserRef.setValue(mCurrentUser)/*updateChildren(mCurrentUser.toMap())*/.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    saveCurrentUserOnSharedPreference();
                    Intent intent = new Intent(UserNameActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}

